package net.jinfm.app.mvvmexample.paging.itemkeydatasource;

import android.util.Log;

import net.jinfm.app.mvvmexample.paging.itemkeydatasource.api.RetrofitClient;
import net.jinfm.app.mvvmexample.paging.itemkeydatasource.model.User;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.paging.ItemKeyedDataSource;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDataSource extends ItemKeyedDataSource<Integer, User> {

    public static final int PER_PAGE = 6;

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<User> callback) {
        // 請求 key 從 0 開始的第一頁數據
//        int since = 0;
//        RetrofitClient.getInstance()
//                .getApi()
//                .getUsers(since, PER_PAGE)
//                .equals(new Callback<List<User>>() {
//                    @Override
//                    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
//                        if (response.body() != null){
//                            callback.onResult(response.body());
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<List<User>> call, Throwable t) {
//                        Log.d("ItemKeydatasource", "onFailure");
//                    }
//                });

        try {

            // 請求 key 從 0 開始的第一頁數據
            int since = 0;
            RetrofitClient.getInstance()
                    .getApi()
//                .getUsers_string(since, PER_PAGE)
                    .getUsers_string()
                    .equals(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.body() != null){
//                            callback.onResult(response.body());
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Log.d("ItemKeydatasource", "onFailure");
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<User> callback) {

        RetrofitClient.getInstance()
                .getApi()
//                .getUsers_string(since, PER_PAGE)
                .getUsers_string()
                .equals(new Callback<List<User>>() {
                    @Override
                    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                        if (response.body() != null){
                            callback.onResult(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<User>> call, Throwable t) {
                        Log.d("ItemKeydatasource", "onFailure");
                    }
                });
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<User> callback) {

    }

    @NonNull
    @Override
    public Integer getKey(@NonNull User item) {
        // 回傳 item 對象的 key
        return item.getId();
    }
}
