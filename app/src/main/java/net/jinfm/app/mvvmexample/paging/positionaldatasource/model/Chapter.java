package net.jinfm.app.mvvmexample.paging.positionaldatasource.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "chapter")
public class Chapter {

    @PrimaryKey()
    @ColumnInfo(name = "chapter_id", typeAffinity = ColumnInfo.TEXT)
    @NonNull
    public String chapter_id;
    @ColumnInfo(name = "title", typeAffinity = ColumnInfo.TEXT)
    public String title;
}
