package net.jinfm.app.mvvmexample.tool;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;


public class MyViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    Application application;
    int data;

    public MyViewModelFactory(Application application, int data) {
        super();
        this.application = application;
        this.data = data;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
//        return super.create(modelClass);
        MyViewModel t = new MyViewModel(application, data);
        return (T) t;
    }

}
