package net.jinfm.app.mvvmexample.databindingex;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import net.jinfm.app.mvvmexample.BR;
import net.jinfm.app.mvvmexample.R;
import net.jinfm.app.mvvmexample.databinding.FragmentDatabindingBinding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * 第八章：DataBinding
 */
public class dataBindingFragment extends Fragment {

    private String TAG = this.getClass().getName();
    private View view;
    private static Context mContext;

    /**
     * XML 布局文件類名：
     * fragment_databinding
     * "Fragment" + "Databinding" + "Binding"
     * --> FragmentDatabindingBinding
     */
    private FragmentDatabindingBinding dataBinding;
    BookModel bookModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // 原本
//        view = inflater.inflate(R.layout.fragment_databinding, container, false);
//        return view;

        // 1. Fragment 綁定 XML 的方式
        dataBinding = FragmentDatabindingBinding.inflate(inflater);

        view = dataBinding.getRoot();
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        init();

        init_dataBinging();

        init_eventHandleListener();

        init_bindingAdapter();

        init_old_new_value();

        init_TwoWayBinding();

        init_recyclerViewDataBinding();

        test_ExtendedFloatingActionButton();

    }

    private void init() {

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        TextView tvAuthor = view.findViewById(R.id.tvAuthor);
        TextView tvRating = view.findViewById(R.id.tvRating);

        // 雙向綁定
        bookModel = new BookModel("Android Jetpack 應用指南", "葉問");
        bookModel.rating = 5;
        // ObservableField
//        bookModel = new BookModel();
//        bookModel.title.set("Android Jetpack 應用指南");
//        bookModel.author.set("葉問");
//        bookModel.rating.set(5);

        tvTitle.setText("書名：" + bookModel.title);
        tvAuthor.setText("作者：" + bookModel.author);
        tvRating.setText("評分：" + bookModel.rating);

    }

    private void init_dataBinging() {

        // 1. 實例化布局後，可直接使用 XML 的元件
        // 若將 XML 元件與資料綁定後，就不需要 setText 了
        dataBinding.tvTitleDatabinding.setText("Binding 書名：" + bookModel.title);
        dataBinding.tvAuthorDatabinding.setText("Binding 作者：" + bookModel.author);
        dataBinding.tvRatingDatabinding.setText("Binding 評分：" + bookModel.rating);

        // 2. 將數據傳遞到 XML
        // DataBinding 自動生成 BR Class，類似 R Class，存放所有 XML 的 id
        // 兩個方法：
        dataBinding.setVariable(BR.bookModel, bookModel);
        dataBinding.setBookModel(bookModel);

    }

    private void init_eventHandleListener(){
        // 在頁面綁定並實例化 EventHandleListener
        dataBinding.setEventHandler(new EventHandleListener(view.getContext(), dataBinding));

    }

    private void init_bindingAdapter(){

        // 自定義 BindingAdapter 網路圖片
        dataBinding.setNetworkImage("https://p.bigstockphoto.com/GeFvQkBbSLaMdpKXF1Zv_bigstock-Aerial-View-Of-Blue-Lakes-And--227291596.jpg");

        // 自定義 BindingAdapter 本地圖片
        dataBinding.setLocalImage(R.mipmap.ic_launcher);

    }

    // 可選舊值
    private void init_old_new_value(){
        dataBinding.setImagePadding(40);
    }

    // 雙向綁定
    private void init_TwoWayBinding(){

        // 雙向綁定
        dataBinding.setViewModel(new TwoWayBindingViewModel());

        // ObservableField 優化雙向綁定
        // 1. 不需要繼承任何類
        // 2. Getter 方法上不需要加上 @Bindable
        // 3. Setter 方法中不需要手動調用 notifyPropertyChanged() 方法
        final LoginModel loginModel = new LoginModel();
        loginModel.userName.set("Michael");
        Log.d(TAG, "loginModel.userName：" + loginModel.userName.get());
        dataBinding.setLoginModel(loginModel);


    }

    // RecyclerView 的綁定機制
    private void init_recyclerViewDataBinding() {
        dataBinding.recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
//        dataBinding.recyclerView.setHasFixedSize(true);

        // 傳參數時，直接處理 click 事件
//        RecyclerViewAdapter adapter = new RecyclerViewAdapter(new RecyclerViewViewModel().getBooks(), new RecyclerViewAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(BookModel item, int position) {
//                Toast.makeText(view.getContext(), "第 " + position + " 個項目", Toast.LENGTH_SHORT).show();
//            }
//        });

        // 傳參數和 click 分開來處理
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(new RecyclerViewViewModel().getBooks());
        adapter.setOnItemClickListener(new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BookModel item, int position) {
                Toast.makeText(view.getContext(), "第 " + position + " 個項目", Toast.LENGTH_SHORT).show();
            }
        });

        dataBinding.recyclerView.setAdapter(adapter);
    }

    private void test_ExtendedFloatingActionButton(){

        dataBinding.nestedscrollviewDatabinding.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                Log.d("20210617", "isExtended: " + dataBinding.myExtendedFloatingActionButton.isExtended());

                if ((scrollY - oldScrollY )<0 && !dataBinding.myExtendedFloatingActionButton.isExtended()) {

                    Log.d("20210617","1");
                    dataBinding.myExtendedFloatingActionButton.extend();

                } else if((scrollY - oldScrollY )>0 && dataBinding.myExtendedFloatingActionButton.isExtended()) {

                    Log.d("20210617","2");
                    dataBinding.myExtendedFloatingActionButton.shrink();

                }
            }
        });

        dataBinding.myExtendedFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!dataBinding.myExtendedFloatingActionButton.isExtended()) {

                    Log.d("20210617","1");
                    dataBinding.myExtendedFloatingActionButton.extend();
//                    dataBinding.myExtendedFloatingActionButton.setExtended(true);

                } else {

                    Log.d("20210617","2");
                    dataBinding.myExtendedFloatingActionButton.shrink();
//                    dataBinding.myExtendedFloatingActionButton.setExtended(false);

                }
            }
        });

//        dataBinding.recyclerView.onscroll

    }

    class FabExtendingOnScrollListener implements NestedScrollView.OnScrollChangeListener {

        @Override
        public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
            Log.d("20210617", "isExtended: " + dataBinding.myExtendedFloatingActionButton.isExtended());

            if ((scrollY - oldScrollY )<0 && !dataBinding.myExtendedFloatingActionButton.isExtended()) {

                Log.d("20210617","1");
                dataBinding.myExtendedFloatingActionButton.extend();

            } else if((scrollY - oldScrollY )>0 && dataBinding.myExtendedFloatingActionButton.isExtended()) {

                Log.d("20210617","2");
                dataBinding.myExtendedFloatingActionButton.shrink();

            }
        }
    }

//    class ExtendedFloatingActionButtonScrollListener(
//            private val floatingActionButton:ExtendedFloatingActionButton
//    ) : RecyclerView.OnScrollListener() {
//        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
//            if (newState == RecyclerView.SCROLL_STATE_IDLE
//                    && !floatingActionButton.isExtended
//                    && recyclerView.computeVerticalScrollOffset() == 0
//            ) {
//                floatingActionButton.extend()
//            }
//            super.onScrollStateChanged(recyclerView, newState)
//        }
//
//        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//            if (dy != 0 && floatingActionButton.isExtended) {
//                floatingActionButton.shrink()
//            }
//            super.onScrolled(recyclerView, dx, dy)
//        }
//    }


}
