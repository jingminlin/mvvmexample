package net.jinfm.app.mvvmexample.paging.testurl;

import net.jinfm.app.mvvmexample.paging.testurl.model.TestUser;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

public class TestUrlDataSourceFactory extends DataSource.Factory<Integer, TestUser>{

    private MutableLiveData<TestUrlDataSource> liveDataSource = new MutableLiveData<>();

    @NonNull
    @Override
    public DataSource<Integer, TestUser> create() {
        TestUrlDataSource dataSource = new TestUrlDataSource();
        liveDataSource.postValue(dataSource);
        return dataSource;
    }
}