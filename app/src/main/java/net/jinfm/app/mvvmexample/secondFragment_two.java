package net.jinfm.app.mvvmexample;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import net.jinfm.app.mvvmexample.tool.TimerViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

public class secondFragment_two extends Fragment {

    private String TAG = this.getClass().getName();
    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_second_two, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();

    }

    private void init() {

        final SeekBar seekBar = view.findViewById(R.id.seekBar);
        // 通過 ViewModelProvider 得到 ViewModel
        // 注意：這裡的參數需要的是 Activity，而不是 Fragment，否則將收不到監聽
        //        TimerViewModel timerViewModel_LiveData_Progress = new ViewModelProvider(this).get(TimerViewModel.class);
        TimerViewModel timerViewModel_LiveData_Progress = new ViewModelProvider(this.getActivity()).get(TimerViewModel.class);
        // 得到 ViewModel 中的 LiveData
        final MutableLiveData<Integer> liveData = (MutableLiveData<Integer>) timerViewModel_LiveData_Progress.getProgress();
        // 方法一：通過 LiveData.observe() 觀察 ViewModel 中的數據變化
//        liveData.observe(getViewLifecycleOwner(), new Observer<Integer>() {
//            @Override
//            public void onChanged(@NonNull Integer integer) {
//                // 收到回調後更新 UI 介面
//                seekBar.setProgress(integer);
//                Log.d(TAG, "21 " + integer);
//            }
//        });
        // 方法二：通過 LiveData.observeForever() 觀察 ViewModel 中的數據變化
        liveData.observeForever(new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                // 收到回調後更新 UI 介面
                seekBar.setProgress(integer);
                Log.d(TAG, "第五章：LiveData Fragment two observeForever " + integer);
                // 用完之後要 removeObserver 來停止觀察，否則不會被回收
//                liveData.removeObserver(this);
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // 當用戶操作 SeekBar 時，更新 ViewModel 中的數據
                liveData.setValue(progress);
                Log.d(TAG, "第五章：LiveData Fragment two seekBar " + progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }


}
