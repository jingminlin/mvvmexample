package net.jinfm.app.mvvmexample.coroutine

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import kotlinx.coroutines.*
import net.jinfm.app.mvvmexample.R
import net.jinfm.app.mvvmexample.VMFactory
import net.jinfm.app.mvvmexample.tool.MyViewModel

class CoroutineActivity  : AppCompatActivity() {

    // https://picapro33.medium.com/%E4%BD%BF%E7%94%A8-kotlin-coroutines-%E9%96%8B%E7%99%BC-android-app-%E5%BF%AB%E9%80%9F%E4%B8%8A%E6%89%8B-77c1b7243c6a

    // A. Job 就是 Coroutines 用來觸發停止的物件
    private var job: Job? = null

    // B. 自己定義 CoroutineScope
    // 預設使用 Dispatchers.Main 也就是在 Android 的 main thread 上執行
    // 第二個參數 Job 就是用來停止這整個 CoroutineScope 的物件
    private val coroutineScope = CoroutineScope(Dispatchers.Main + Job())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coroutine)

        // A. 過 GlobalScope.launch 產生一個 coroutine，它會依照 Dispatcher.IO 的指示執行在 background thread
        job = GlobalScope.launch(Dispatchers.IO) {
            // Do something in the background
        }

        // B. 自己定義 CoroutineScope
        // 這邊沒有去接回傳的 Job
        // 是因為 Kotlin Coroutines 會將 child job 連結到 parent job
        // 當 parent job 被 cancel 時，所有的 child jobs 也會一併被 cancel
        coroutineScope.launch(Dispatchers.IO) {
            // Do something in the background
        }

        initView()

        testVMFactory()

    }

    override fun onDestroy() {
        job?.cancel()
        super.onDestroy()
    }

    fun initView() {

        val fragment = CoroutineFrament.newInstance(this)
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_layout, fragment, CoroutineFrament::class.java.simpleName)
//                .addToBackStack(null)
                .commitAllowingStateLoss()

    }

    fun testVMFactory() {

        // 測試這個 https://akiwang.com/blog/20201007_android_mvvm_view_model_factory/
        val myViewModelFactory: ViewModel =
                ViewModelProviders.of(this,
                        VMFactory { MyViewModel(this.application, 888) })
                        .get(MyViewModel::class.java)

        val liveData = (myViewModelFactory as MyViewModel).currentSecond as MutableLiveData<Int>
        // 方法一：通過 LiveData.observe() 觀察 ViewModel 中的數據變化
        liveData.observe(this, {
            // 只有在 Lifecycle.State.ON_STARTED 或 Lifecycle.State.ON_RESUME 時，頁面才能收到來自 LiveDate 的通知
                Log.d("CoroutineActivity", "第五章：LiveData 傳遞參數給 ViewModelFactory observe " + it);
        })
    }
}