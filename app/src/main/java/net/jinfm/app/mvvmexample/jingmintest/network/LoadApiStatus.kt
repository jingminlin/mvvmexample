package net.jinfm.app.mvvmexample.jingmintest.model

enum class LoadApiStatus {
    LOADING,
    ERROR,
    DONE
}