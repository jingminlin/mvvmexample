package net.jinfm.app.mvvmexample.databindingex;

import androidx.databinding.ObservableField;

public class LoginModel {
    public final ObservableField<String> userName = new ObservableField<>();
}
