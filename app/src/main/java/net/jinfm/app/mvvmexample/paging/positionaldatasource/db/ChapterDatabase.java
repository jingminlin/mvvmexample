package net.jinfm.app.mvvmexample.paging.positionaldatasource.db;

import android.content.Context;

import net.jinfm.app.mvvmexample.paging.positionaldatasource.model.Chapter;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Chapter.class}, version = 1)//版本是要幹嘛的？
public abstract class ChapterDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "chapter_db";

    private static ChapterDatabase databaseInstance;

    /**
     * 單例、同步、鎖
     */
    public static synchronized ChapterDatabase getInstance(Context context) {
        if (databaseInstance == null) {

            databaseInstance = Room.databaseBuilder(
                    context.getApplicationContext(), ChapterDatabase.class, DATABASE_NAME)
                    /**
                     * 第六章：Room 資料庫升級
                     * 若有錯誤 Room 會拋出 IllegalStateException
                     * Room 會直接把 table 重建，原本的資料會全部清空，除非還在內部開發中，否則一般都不建議這樣做
                     */
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries() // //其實這種耗時的動作應該丟到背景去執行，特殊原因要強制就家這句
                    .build();
        }
        return databaseInstance;
    }

    /**
     * 以抽象方法返回 Dao 物件
     */
    public abstract ChapterDao chapterDao();

}
