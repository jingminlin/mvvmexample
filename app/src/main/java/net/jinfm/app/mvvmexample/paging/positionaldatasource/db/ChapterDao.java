package net.jinfm.app.mvvmexample.paging.positionaldatasource.db;

import net.jinfm.app.mvvmexample.paging.positionaldatasource.model.Chapter;

import java.util.List;

import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface ChapterDao {

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    void insertChapters(List<Chapter> chapters);

    @Query("DELETE FROM chapter")
    void clear();

    @Query("SELECT * FROM chapter")
    DataSource.Factory<Integer, Chapter> getChapterList();

    @Query("SELECT * FROM chapter WHERE chapter_id = :chapter_id")
    Chapter getChapterByChapterId(int chapter_id);
}
