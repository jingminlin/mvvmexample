package net.jinfm.app.mvvmexample.workmanager;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class UploadLogWorker_1 extends Worker {

    private String TAG = this.getClass().getName();
    private int count = 0;

    public UploadLogWorker_1(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    /**
     * 第七章：WorkManager
     * 耗時的任務在 doWork() 方法中執行
     */
    @NonNull
    @Override
    public Result doWork() {
        /**
         * 三種類型的返回值
         * 成功 Result.success()
         * 失敗 Result.failure()
         * 需要重新執行 Result.retry()
         */

        if (getInputData().getString("input_data") != null) {

            // 接收數據
            String inputData = getInputData().getString("input_data");

            Log.d(TAG, "第七章：WorkManager -> inputData：" + inputData);

            // 回傳數據
            Data outputData = new Data.Builder()
                    .putString("output_data", "Task Success!")
                    .build();

            return Result.success(outputData);

        } else {

            // 週期性任務
            Log.d(TAG, "第七章：WorkManager 週期性任務 開始執行");
//            if (count == 0) {
//                Log.d(TAG, "第七章：WorkManager 週期性任務 開始執行：" + count);
//            } else {
//                Log.d(TAG, "第七章：WorkManager 週期性任務 重試第 " + count + " 次");
//            }
//
//            count = count + 1;

            // 若 BackoffPolicy.LINEAR＋30
            // 則每次重試的時間長度為 30秒 -> 60秒 -> 90秒 -> 120秒 以此列推
            return Result.retry();

        }

    }
}
