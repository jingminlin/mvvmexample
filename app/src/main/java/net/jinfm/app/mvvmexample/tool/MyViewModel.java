package net.jinfm.app.mvvmexample.tool;

import android.app.Application;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import static android.content.ContentValues.TAG;


public class MyViewModel extends AndroidViewModel {

    private Timer timer_LiveData;
//    private MutableLiveData<Integer> data_LiveData = new MutableLiveData<>();

    public MyViewModel(@NonNull Application application, int data) {
        super(application);
        if (data_LiveData == null) {
            data_LiveData = new MutableLiveData<>();
        }
        data_LiveData.setValue(data);

    }

    private MutableLiveData<Integer> data_LiveData;
    public LiveData<Integer> getCurrentSecond() {
        if (data_LiveData == null) {
            data_LiveData = new MutableLiveData<>();
        }
        startTiming_LiveData();
        return data_LiveData;
    }

    public void startTiming_LiveData() {
        if (timer_LiveData == null) {
//            data_LiveData.setValue(0);
            timer_LiveData = new Timer();
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    data_LiveData.postValue(data_LiveData.getValue() + 1);
                }
            };
            timer_LiveData.schedule(timerTask, 1000, 1000);
        }
    }


    /**
     * 清理資源
     */
    @Override
    protected void onCleared() {
        super.onCleared();
        try {
            Log.d(TAG, "onCleared");
            timer_LiveData.cancel();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}