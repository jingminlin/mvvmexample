package net.jinfm.app.mvvmexample.paging.positionaldatasource;

import android.util.Log;

import net.jinfm.app.mvvmexample.paging.positionaldatasource.api.RetrofitClient;
import net.jinfm.app.mvvmexample.paging.positionaldatasource.model.Chapter;
import net.jinfm.app.mvvmexample.paging.positionaldatasource.model.Chapters;

import androidx.annotation.NonNull;
import androidx.paging.PositionalDataSource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 第九章：Paging PositionalDataSource
 */
public class ChapterDataSource extends PositionalDataSource<Chapter> {

//    private int amount_per_page = 10;
    public static final int amount_per_page = 10;
    private int page = 1;

//    public ChapterDataSource(int amount_per_page, int page) {
//        this.amount_per_page = amount_per_page;
//        this.page = page;
//    }

    /**
     * 頁面首次加載數據時會調用
     * 通過 callback.onResult() 方法將數據返回給 PagedList
     * @param params
     * @param callback
     */
    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback<Chapter> callback) {
        RetrofitClient.getInstance()
                .getApi()
                .getChapters(212910, "chapter", amount_per_page, page)
                .enqueue(new Callback<Chapters>() {
                    @Override
                    public void onResponse(Call<Chapters> call, Response<Chapters> response) {
                        if (response.body() != null){
                            /**
                             * 第三個參數 totalCount
                             * 若 PagedList.Config 中設置了 setEnablePlaceholders() 方法為 true (預設為 true)
                             * 那麼需告知 PagedList totalCount 為多少，讓 RecyclerView 預留位置
                             * 若沒告知會 Error
                             * 因此該功能只適用於數據量確定的時候使用，且數據量不能太大，否則會消耗部必要的性能
                             */
//                            callback.onResult(response.body().chapterList,
//                                    response.body().start,
//                                    response.body().total);
//                            callback.onResult(response.body().chapterList,
//                                    (amount_per_page * (page-1)),
//                                    40); // 所有的筆數，若 API 有回傳總數的話可以使用

//                            callback.onResult(response.body().chapterList, 0, 999); // 若超過真實資料數量，會閃退
                            callback.onResult(response.body().chapterList, 0);
                            Log.d("paging","第九章：paging loadInitial " + page);
                            page = page + 1;


                        }
                    }

                    @Override
                    public void onFailure(Call<Chapters> call, Throwable t) {

                    }
                });
    }

    /**
     * 加載下一頁的工作會在 loadRange() 方法內進行
     * @param params
     * @param callback
     */
    @Override
    public void loadRange(@NonNull LoadRangeParams params, @NonNull LoadRangeCallback<Chapter> callback) {
        RetrofitClient.getInstance()
                .getApi()
                .getChapters(212910, "chapter", amount_per_page, page)
                .enqueue(new Callback<Chapters>() {
                    @Override
                    public void onResponse(Call<Chapters> call, Response<Chapters> response) {
                        if (response.body() != null){
                            Log.d("paging","第九章：paging loadRange " + page);
                            callback.onResult(response.body().chapterList);
                            page = page + 1;
                        }
                    }

                    @Override
                    public void onFailure(Call<Chapters> call, Throwable t) {

                    }
                });
    }
}
