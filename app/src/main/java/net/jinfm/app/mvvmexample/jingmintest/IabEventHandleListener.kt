package net.jinfm.app.mvvmexample.jingmintest

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import net.jinfm.app.mvvmexample.MainFragment
import net.jinfm.app.mvvmexample.R
import net.jinfm.app.mvvmexample.databinding.FragmentIabBinding

class IabEventHandleListener(private val context: Context?, dataBinding: FragmentIabBinding) {
//    , private val activity: Activity?
    private val dataBinding: FragmentIabBinding
    private var activity: Activity

    companion object {
        private const val TAG = "IabEventHandleListener"
    }

    init {
        this.dataBinding = dataBinding
        this.activity = context as Activity
    }

    fun onIconBackClicked(view: View?) {
        Log.d(TAG, "onIconBackClicked")
        (context as AppCompatActivity).onBackPressed()
    }

    fun onIconQuestionClicked(view: View?) {
        Log.d(TAG, "onIconQuestionClicked")

//        val fragment: Fragment = QuestionFragment.newInstance(activity)
//        val ft: FragmentTransaction = (context as AppCompatActivity).supportFragmentManager.beginTransaction()
//        ft.setCustomAnimations(R.anim.fragment_slide_under_enter, 0, 0, R.anim.fragment_slide_under_exit)
//        ft.add(R.id.iab_questionlayout, fragment, QuestionFragment::class.java.simpleName)
//        ft.addToBackStack(IabFragment::class.java.simpleName)
//        ft.commit()

        val fragment: Fragment = QuestionFragment.newInstance(activity)
        val ft: FragmentTransaction = (context as AppCompatActivity).getSupportFragmentManager().beginTransaction()
        ft.replace(R.id.iab_questionlayout, fragment)
        ft.commit()
    }

//    fun onButtonClicked(view: View?, rating: Int) {
//        Toast.makeText(context, "I am clicked $rating", Toast.LENGTH_SHORT).show()
//    }
//
//    fun onImageClicked(view: View?) {
//        dataBinding.setNetworkImage(null)
//    }
//
//    // 可選舊值
//    fun onClickedChangeImagePadding(view: View?) {
//        dataBinding.setImagePadding(180)
//    }
//
//    fun onClickedSetEditText(view: View?) {
//        dataBinding.getLoginModel().userName.set("test text")
//    }

}
