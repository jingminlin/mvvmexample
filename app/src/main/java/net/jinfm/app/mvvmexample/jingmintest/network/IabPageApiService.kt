package net.jinfm.app.mvvmexample.jingmintest.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import net.jinfm.app.mvvmexample.jingmintest.model.NewsPaperData
import net.jinfm.app.mvvmexample.jingmintest.model.ServerIABData
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url
import java.util.concurrent.TimeUnit

private const val BASE_URL_IAB = "https://2ld5vp3gx6.execute-api.ap-northeast-1.amazonaws.com/Prod/hello/"
private const val BASE_URL_vipProducts = "https://lab7-misc.udn.com/AppFeedMaker/"

private const val BASE_URL_DATE = "https://lab7-misc.udn.com/AppFeedMaker/vippaper/"

private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

private val client = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        })
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(15, TimeUnit.SECONDS)
        .readTimeout(10, TimeUnit.SECONDS)
        .build()

private val retrofit = Retrofit.Builder()
//        .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
        .addConverterFactory(MoshiConverterFactory.create(moshi))
//        .addConverterFactory(MoshiConverterFactory.create())
//        .addConverterFactory(MoshiConverterFactory.create().asLenient())
//        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl(BASE_URL_vipProducts)
//        .client(client)
        .build()

private val retrofit_date = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .baseUrl(BASE_URL_DATE)
        .client(client)
        .build()

interface IabPageApiService {

//    @GET
//    fun getBorrowNum(@Url url: String?): Call<ResponseBody?>?
//
//    @POST
//    fun postBorrowRecords(@Url url: String?,
//                          @Header("Authorization") authorization: String?,
//                          @Body borrowRecord: RequestBody?): Call<ResponseBody?>?
//
//    @GET("vipDate")
//    suspend fun getPaperDates(): List<DateItem>

    @GET("vipProducts")
    suspend fun getServerIabData(): ServerIABData

//    @GET("vipProducts")
//    fun getServerIabData_string(): Call<String>

    @GET("vipProducts")
    fun getServerIabData_string(): String

    @GET
    fun getServerIabData_test(@Url url: String?): Call<ResponseBody?>?

    @GET("2021-04-10/")
    suspend fun getNewsPapers(): List<NewsPaperData>

}

object IabPageApi {
    val retrofitService: IabPageApiService by lazy { retrofit.create(IabPageApiService::class.java) }
}

object NewsPaperApi {
    val retrofitService: IabPageApiService by lazy { retrofit_date.create(IabPageApiService::class.java) }
}
