package net.jinfm.app.mvvmexample.paging.testurl;

import android.util.Log;

import net.jinfm.app.mvvmexample.paging.testurl.api.TestRetrofitClient;
import net.jinfm.app.mvvmexample.paging.testurl.model.TestUser;
import net.jinfm.app.mvvmexample.paging.testurl.model.TestUsers;

import androidx.annotation.NonNull;
import androidx.paging.ItemKeyedDataSource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TestUrlDataSource extends ItemKeyedDataSource<Integer, TestUser> {

    public static final int PER_PAGE = 6;

    @Override
    public void loadInitial(@NonNull ItemKeyedDataSource.LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<TestUser> callback) {
        // 請求 key 從 0 開始的第一頁數據
        int since = 0;
        TestRetrofitClient.getInstance()
                .getApi()
                .getUsers(since, PER_PAGE)
//                .getUsers()
                .equals(new Callback<TestUsers>() {
                    @Override
                    public void onResponse(Call<TestUsers> call, Response<TestUsers> response) {
                        if (response.body() != null){
                            callback.onResult(response.body().TestUserList);
                        }
                    }

                    @Override
                    public void onFailure(Call<TestUsers> call, Throwable t) {
                        Log.d("ItemKeydatasource", "onFailure");
                    }
                });
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<TestUser> callback) {

        TestRetrofitClient.getInstance()
                .getApi()
                .getUsers(params.key, PER_PAGE)
//                .getUsers()
                .equals(new Callback<TestUsers>() {
                    @Override
                    public void onResponse(Call<TestUsers> call, Response<TestUsers> response) {
                        if (response.body() != null){
                            callback.onResult(response.body().TestUserList);
                        }
                    }

                    @Override
                    public void onFailure(Call<TestUsers> call, Throwable t) {
                        Log.d("ItemKeydatasource", "onFailure");
                    }
                });
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<TestUser> callback) {

    }

    @NonNull
    @Override
    public Integer getKey(@NonNull TestUser item) {
        // 回傳 item 對象的 key
        return item.id;
    }
}
