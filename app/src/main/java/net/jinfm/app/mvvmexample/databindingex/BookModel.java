package net.jinfm.app.mvvmexample.databindingex;

/**
 * 第八章：DataBinding
 */
public class BookModel {

    public String title;
    public String author;
    public int rating;
    public String image;

    public BookModel(String title, String author) {
        this.title = title;
        this.author = author;
    }

    // ObservableField
//    public final ObservableField<String> title = new ObservableField<>();
//    public final ObservableField<String> author = new ObservableField<>();
//    public final ObservableInt rating = new ObservableInt();

}
