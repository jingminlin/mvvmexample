package net.jinfm.app.mvvmexample.tool;

import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

/**
 * 第二章：LifeCycle
 */
public class ApplicationObserver implements LifecycleObserver {

    private String TAG = this.getClass().getName();

    /**
     * 在應用程序的整個生命週期中只會被調用一次
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public void onCreate() {
        Log.d(TAG, "第二章：Application Lifecycle.Event.ON_CREATE");
    }

    /**
     * 當應用程序在前台出現時被調用
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onStart() {
        Log.d(TAG, "第二章：Application Lifecycle.Event.ON_START");
    }

    /**
     * 當應用程序在前台出現時被調用
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onResume() {
        Log.d(TAG, "第二章：Application Lifecycle.Event.ON_RESUME");
    }

    /**
     * 當應用程序退出到後台時被調用
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void onPause() {
        Log.d(TAG, "第二章：Application Lifecycle.Event.ON_PAUSE");
    }

    /**
     * 當應用程序退出到後台時被調用
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onStop() {
        Log.d(TAG, "第二章：Application Lifecycle.Event.ON_STOP");
    }

    /**
     * 永遠不會被調用，系統不會分發調用 ON_DESTROY 事件
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy() {
        Log.d(TAG, "第二章：Application Lifecycle.Event.ON_DESTROY");
    }

}
