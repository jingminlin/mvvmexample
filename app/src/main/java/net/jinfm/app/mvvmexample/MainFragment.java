package net.jinfm.app.mvvmexample;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.widget.TextView;

import net.jinfm.app.mvvmexample.room.MyDatabase;
import net.jinfm.app.mvvmexample.room.QueryStudentTask_LiveData;
import net.jinfm.app.mvvmexample.room.Student;
import net.jinfm.app.mvvmexample.room.StudentViewModel;
import net.jinfm.app.mvvmexample.tool.MyLocationListener;
import net.jinfm.app.mvvmexample.tool.MyNotification;
import net.jinfm.app.mvvmexample.tool.MyService;
import net.jinfm.app.mvvmexample.tool.TimerViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

public class MainFragment extends Fragment {

    private String TAG = this.getClass().getName();
    private View view;
    private static Context mContext;

    private MyLocationListener myLocationListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main, container, false);
        if (mContext == null && view != null) {
            mContext = view.getContext();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initObserver();

        initNavigation();

        initComponent();

        testFunction();

        // 實例化，並監聽 LiveData 變化 --------------------
        StudentViewModel studentViewModel = new ViewModelProvider(this).get(StudentViewModel.class);
        studentViewModel.getLiveDataStudent().observe(getViewLifecycleOwner(), new Observer<List<Student>>() {
            @Override
            public void onChanged(List<Student> students) {
                Log.d(TAG, "" + students.size());
            }
        });
        MyDatabase myDatabase = MyDatabase.getInstance(getContext());
        QueryStudentTask_LiveData ueryStudentTask_LiveData = new QueryStudentTask_LiveData(getContext());
        ueryStudentTask_LiveData.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"Main");

    }

    private void testFunction() {

        view.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
            @Override
            public WindowInsets onApplyWindowInsets(View v, WindowInsets insets) {
//                int statusBarSize = insets.systemWindowInsetTop()
                int statusBarSize = insets.getSystemWindowInsetTop();
                Log.d("*** Elenasys :: ", "statusBarSize: " + statusBarSize);
                return insets;
            }
        });

//        net.jinfm.app.mvvmexample.paging.pagekeyeddatasource.UserViewModel tttt = new ViewModelProvider(getActivity()).get(net.jinfm.app.mvvmexample.paging.pagekeyeddatasource.UserViewModel.class);
//        tttt.UserPagedList.observe(getActivity(), new Observer<PagedList<net.jinfm.app.mvvmexample.paging.pagekeyeddatasource.model.User>>() {
//            @Override
//            public void onChanged(PagedList<net.jinfm.app.mvvmexample.paging.pagekeyeddatasource.model.User> users) {
//                Log.d(TAG, "jingmin users " + users.toString());
//            }
//        });

//        net.jinfm.app.mvvmexample.paging.itemkeydatasource.UserViewModel userViewModel =
//                new ViewModelProvider(getActivity()).get(UserViewModel.class);
//        userViewModel.UserPagedList.observe(getActivity(), new Observer<PagedList<User>>() {
//            @Override
//            public void onChanged(PagedList<User> users) {
//                Log.d(TAG, "jingmin users " + users.toString());
//            }
//        });

//        TestUrlViewModel testUrlViewModel =
//                new ViewModelProvider(getActivity()).get(TestUrlViewModel.class);
//        testUrlViewModel.testPagedList.observe(getViewLifecycleOwner(), new Observer<PagedList<TestUser>>() {
//            @Override
//            public void onChanged(PagedList<TestUser> users) {
//
//                Log.d(TAG, "jingmin users " + users.toString());
//            }
//        });

    }

    private void initNavigation() {

        // Navigation 換頁參數
        Bundle bundle = new MainFragmentArgs.Builder()
                .setUserName("Michael")
                .setAge(30)
                .build().toBundle();

        // Navigation 換頁方法一：
        view.findViewById(R.id.btnToSecondFragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_secondFragment);
                Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_secondFragment, bundle);
            }
        });

        // Navigation 換頁方法二：
//        view.findViewById(R.id.btnToSecondFragment)
//                .setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_mainFragment_to_secondFragment
//                , bundle));

        /**
         * 第三章 Navigation ------------------------------------------------------------------------
         * 深層連結 DeepLink
         * PendingIntent 方式
         */
        view.findViewById(R.id.btnNotification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "模擬 Push");
                MyNotification myNotification = new MyNotification(mContext);
                myNotification.sendNotification();
            }
        });


        /**
         * 第六章：Room
         */
        // Navigation 換頁方法一：
        view.findViewById(R.id.btnToRoomFragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_roomFragment);
//                Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_roomFragment, bundle);
            }
        });

        /**
         * 第八章：DataBinding ------------------------------------------------------------------------
         *
         */
        view.findViewById(R.id.btnDataBinding).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_dataBindingFragment);
//                Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_roomFragment, bundle);
            }
        });

        // 前往測試 Coroutine ------------------------------------------------------------------------
        view.findViewById(R.id.btnToCoroutine).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).toCoroutines();
            }
        });

        // 前往 fingmin test ------------------------------------------------------------------------
        view.findViewById(R.id.btnToJingminTest).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).toJingminTest(v);
//                Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_jingminTestFragment);
            }
        });

        // 前往 fingmin Paging ------------------------------------------------------------------------
        view.findViewById(R.id.btnToPaging).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_PagingFragment);

//                Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_ItemKeyFragment);
//                Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_ItemKeyDemoFragment);

            }
        });

    }

    /**
     * 第二章：LifeCycle
     */
    private Intent intent;
    private void initObserver() {

        myLocationListener = new MyLocationListener(view.getContext(), "MainFragment", new MyLocationListener.OnLocationChangedListener() {
            @Override
            public void onChanged(int data) {

                Log.d(TAG, "第二章：onChanged 展示收到的位置訊息 " + data);

            }
        });

        // 將觀察者與被觀察者綁定
        getLifecycle().addObserver(myLocationListener);

        view.findViewById(R.id.btnStartService).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 啟動服務
                intent = new Intent(mContext, MyService.class);
                mContext.startService(intent);
                Log.d(TAG, "第二章：啟動服務");
            }
        });

        view.findViewById(R.id.btnStoptService).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 停止服務
                if (intent != null) {
                    mContext.stopService(intent);
                    Log.d(TAG, "第二章：停止服務");
                }
            }
        });

    }

    private void initComponent(){

        // 第四章：ViewModel ---------
        final TextView tvTime = view.findViewById(R.id.tvTime);
        TimerViewModel timerViewModel = new ViewModelProvider(this).get(TimerViewModel.class);
        timerViewModel.setOnTimeChangeListener(new TimerViewModel.OnTimeChangeListener() {
            @Override
            public void onTimeChanged(int second) {
                try{
                    // 更新 UI 介面
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvTime.setText("Time :" + second);
                        }
                    });
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        timerViewModel.startTiming();

        // ---------------------

        // 第五章：LiveData ---------
        final TextView tvTime_LiveData = view.findViewById(R.id.tvTime_LiveData);
        // 通過 ViewModelProvider 得到 ViewModel
        // 注意：在不同 Fragment 之間，這裡的參數需要的是 Activity，而不是 Fragment，否則將收不到監聽
//        TimerViewModel timerViewModel_LiveData = new ViewModelProvider(this).get(TimerViewModel.class);
        TimerViewModel timerViewModel_LiveData = new ViewModelProvider(this.getActivity()).get(TimerViewModel.class);
        // 直接觀察 LiveData
//        timerViewModel_LiveData.currentSecond_LiveData.observe(getViewLifecycleOwner(), new Observer<Integer>() {
//            @Override
//            public void onChanged(Integer integer) {
//
//            }
//        });
        // 得到 ViewModel 中的 LiveData
        final MutableLiveData<Integer> liveData = (MutableLiveData<Integer>) timerViewModel_LiveData.getCurrentSecond();
        // 方法一：通過 LiveData.observe() 觀察 ViewModel 中的數據變化
        liveData.observe(getViewLifecycleOwner(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                // 收到回調後更新 UI 介面
                tvTime_LiveData.setText("Time :" + integer);
                // 只有在 Lifecycle.State.ON_STARTED 或 Lifecycle.State.ON_RESUME 時，頁面才能收到來自 LiveDate 的通知
//                Log.d(TAG, "第五章：LiveData observe " + integer);
            }
        });
        // 方法二：通過 LiveData.observeForever() 觀察 ViewModel 中的數據變化
//        liveData.observeForever(new Observer<Integer>() {
//            @Override
//            public void onChanged(@Nullable Integer integer) {
//                tvTime_LiveData.setText("Time :" + integer);
//                // Lifecycle.State.ON_STARTED 或 Lifecycle.State.ON_RESUME 和其他狀態，頁面都能收到來自 LiveDate 的通知
//                Log.d(TAG, "observeForever" + integer);
//                liveData.removeObserver(this);
//            }
//        });

        tvTime_LiveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 通過 LiveData.setValue() / LiveData.postValue()
                // 更新 ViewModel 數據
                liveData.setValue(0); // UI 線程中
//                liveData.postValue(0); // 非 UI 線程中
            }
        });
        timerViewModel_LiveData.startTiming_LiveData();
        // ---------------------

    }

    public static MainFragment newInstance(Context _mContext) {

        MainFragment fragment = new MainFragment();

        mContext = _mContext;

        return fragment;

    }

}
