package net.jinfm.app.mvvmexample.paging.itemkeydemo

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import net.jinfm.app.mvvmexample.R
import net.jinfm.app.mvvmexample.databinding.FragmentItemkeydemoBinding
import net.jinfm.app.mvvmexample.paging.itemkeydemo.model.ItemKeyDemoViewModel

class ItemkeyDemoFragment  : Fragment() {

    lateinit var dataBinding: FragmentItemkeydemoBinding

    private val viewModel: ItemKeyDemoViewModel by lazy(LazyThreadSafetyMode.NONE) {
        ViewModelProviders.of(this).get(ItemKeyDemoViewModel::class.java)
    }
//    private val adapter: ItemkeyDemoAdapter = ItemkeyDemoAdapter()
    private var adapter: ItemkeyDemoAdapter? = null

    companion object {
        private const val TAG = "ItemkeyDemoFragment"
        private var activity: Activity? = null

        @JvmStatic
        fun newInstance(activity: Activity): ItemkeyDemoFragment {
            val ItemkeyDemoFragment = ItemkeyDemoFragment()
            this.activity = activity
            return ItemkeyDemoFragment
        }

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        dataBinding = FragmentItemkeydemoBinding.inflate(inflater, container, false)
        dataBinding.lifecycleOwner = this

        return dataBinding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        initHandler()
    }

    private fun initView() {

        // 分隔線
//        view_item_list.addItemDecoration(DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL))
//        view_item_list.setHasFixedSize(true)
//        adapter = ItemkeyDemoAdapter(context)
////        view_item_list.adapter = adapter
//        view_item_list.setAdapter(adapter)

        adapter = ItemkeyDemoAdapter(context)
        var view_item_list: RecyclerView = dataBinding.root.findViewById(R.id.view_item_list)
        view_item_list.setLayoutManager(LinearLayoutManager(context))
        view_item_list.setHasFixedSize(true)
        view_item_list.setAdapter(adapter)

        getActivity()?.let {
            viewModel.getItems().observe(it, Observer { items ->
                items?.let {
                    adapter!!.submitList(items)
                }
            })
        }
    }

    private fun initHandler() {

    }

    fun getActivity(context: Context?): Activity? {
        if (context == null) {
            return null
        } else if (context is ContextWrapper) {
            return if (context is Activity) {
                context
            } else {
                getActivity(context.baseContext)
            }
        }
        return null
    }





}
