package net.jinfm.app.mvvmexample.tool;

import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

/**
 * 第二章：LifeCycle
 */
public class MyServiceObserver implements LifecycleObserver {

    private String TAG = this.getClass().getName();

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private void createGetLocation() {
        Log.d(TAG, "第二章：MyServiceObserver startGetLocation 當 Server 的 ON_CREATE() 執行時會被調用");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void startGetLocation() {
        Log.d(TAG, "第二章：MyServiceObserver stopGetLocation 當 Server 的 ON_START() 執行時會被調用");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private void pauseGetLocation() {
        Log.d(TAG, "第二章：MyServiceObserver stopGetLocation 當 Server 的 ON_PAUSE() 執行時會被調用");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void stopGetLocation() {
        Log.d(TAG, "第二章：MyServiceObserver stopGetLocation 當 Server 的 ON_DESTROY() 執行時會被調用");
    }

}
