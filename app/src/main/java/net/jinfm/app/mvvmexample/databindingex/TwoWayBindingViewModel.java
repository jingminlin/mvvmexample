package net.jinfm.app.mvvmexample.databindingex;

import net.jinfm.app.mvvmexample.BR;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

/**
 * 第八章：DataBinding
 * BaseObservable 雙向綁定
 * ObservableField 優化雙向綁定
 */
public class TwoWayBindingViewModel extends BaseObservable {

    private BookModel bookModel;

    public TwoWayBindingViewModel () {
        bookModel = new BookModel("Android Jetpack 應用指南", "葉問");
        bookModel.rating = 5;
    }

    // Getter 方法，@Bindable 對這個字段進行雙向綁定
    @Bindable
    public String getTitle(){
        return bookModel.title;
    }

    // Setter 會自動被調用
    public void setTitle(String title) {
        // 注意！要判斷新值與舊值是否不同，因為在更新後會通知觀察者，數據已更新
        // 觀察者在收到通知後，會對 Setter 方法進行調用
        // 若沒有對值進行判斷，則會引發循環調用的問題
        if (title != null && !title.equals(bookModel.title)) {
            bookModel.title = title;
            // 處理一些業務相關邏輯，例如保存 title
            // notifyPropertyChanged() 這是 BaseObservable 類中的一個方法
            notifyPropertyChanged(BR.title);
        }
    }
}

