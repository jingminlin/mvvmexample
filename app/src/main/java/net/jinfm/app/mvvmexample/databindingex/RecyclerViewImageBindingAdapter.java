package net.jinfm.app.mvvmexample.databindingex;

import android.graphics.Color;
import android.text.TextUtils;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import net.jinfm.app.mvvmexample.R;

import androidx.databinding.BindingAdapter;

/**
 * 第八章：DataBinding
 * RecyclerView 的綁定機制
 */
public class RecyclerViewImageBindingAdapter {

    @BindingAdapter("itemImage")
    public static void setItemImage(ImageView imageView, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.get()
                    .load(imageUrl)
                    .placeholder(R.drawable.imgpsh_fullsize)
                    .error(R.drawable.imgpsh_fullsize)
                    .into(imageView);
        } else {
            imageView.setBackgroundColor(Color.DKGRAY);
        }
    }

}
