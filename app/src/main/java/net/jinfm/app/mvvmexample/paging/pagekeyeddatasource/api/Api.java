package net.jinfm.app.mvvmexample.paging.pagekeyeddatasource.api;

import net.jinfm.app.mvvmexample.paging.pagekeyeddatasource.model.UserResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * 第九章：Paging
 */
public interface Api {
    @GET("users")
    Call<UserResponse> getUsers(
            @Query("page") int page,
            @Query("pagesize") int pagesize,
            @Query("site") String site
    );
}