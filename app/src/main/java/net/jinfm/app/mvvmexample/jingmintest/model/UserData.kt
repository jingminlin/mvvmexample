package net.jinfm.app.mvvmexample.jingmintest.model


import android.annotation.SuppressLint
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class UserData(
    @Json(name = "address")
    val address: Address,
    @Json(name = "company")
    val company: Company,
    @Json(name = "email")
    val email: String,
    @Json(name = "id")
    val id: Int,
    @Json(name = "name")
    val name: String,
    @Json(name = "phone")
    val phone: String,
    @Json(name = "username")
    val username: String,
    @Json(name = "website")
    val website: String
) : Parcelable {
    @SuppressLint("ParcelCreator")
    @Parcelize
    data class Address(
        @Json(name = "city")
        val city: String,
        @Json(name = "geo")
        val geo: Geo,
        @Json(name = "street")
        val street: String,
        @Json(name = "suite")
        val suite: String,
        @Json(name = "zipcode")
        val zipcode: String
    ) : Parcelable {
        @SuppressLint("ParcelCreator")
        @Parcelize
        data class Geo(
            @Json(name = "lat")
            val lat: String,
            @Json(name = "lng")
            val lng: String
        ) : Parcelable
    }

    @SuppressLint("ParcelCreator")
    @Parcelize
    data class Company(
        @Json(name = "bs")
        val bs: String,
        @Json(name = "catchPhrase")
        val catchPhrase: String,
        @Json(name = "name")
        val name: String
    ) : Parcelable
}