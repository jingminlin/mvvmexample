package net.jinfm.app.mvvmexample

class Totaller(var total: Int = 0) {
    fun add(num: Int): Int {
        total = total + num
        return total
    }
}