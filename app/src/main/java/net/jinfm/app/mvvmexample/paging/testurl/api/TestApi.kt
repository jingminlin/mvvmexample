package net.jinfm.app.mvvmexample.paging.testurl.api

import net.jinfm.app.mvvmexample.paging.testurl.model.TestUser
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

public interface TestApi {
    /**
     * 獲取目錄資料
     */
//    @GET("ShowStoreProductChapter")
//    open fun getChapters(
//            @Query("id") id: Int,
//            @Query("order_by") order_by: String,
//            @Query("amount_per_page") amount_per_page: Int,
//            @Query("page") page: Int
//    ): Call<Chapters>

    @GET("users")
    fun getUsers(
            @Query("since") since: Int,
            @Query("per_page") perPage: Int
    ): Call<List<TestUser?>?>?

    @GET("users")
    fun getUsers(): Call<List<TestUser?>?>?
}