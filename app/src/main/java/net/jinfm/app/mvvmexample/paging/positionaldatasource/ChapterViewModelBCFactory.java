package net.jinfm.app.mvvmexample.paging.positionaldatasource;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class ChapterViewModelBCFactory extends ViewModelProvider.NewInstanceFactory {

    Application application;

    public ChapterViewModelBCFactory(Application application) {
        super();
        this.application = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
//        return super.create(modelClass);
        ChapterViewModelBC t = new ChapterViewModelBC(application);
        return (T) t;
    }

}