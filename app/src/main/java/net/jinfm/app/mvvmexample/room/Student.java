package net.jinfm.app.mvvmexample.room;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

/**
 * 第六章：Room
 * @Entity 用於將 Student 類與 Room 中的 Table 對應起來
 * tableName 可以為 Table 設置 name，若不設置，則 Table name 與 class name 相同
 */
@Entity(tableName = "student")
public class Student {

    /**
     * @PrimaryKey 指定該字段為 table 的主鍵
     * @ColumnInfo 設置該字段儲存在 Table 中的欄位名稱，並指定類型
     */
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id", typeAffinity = ColumnInfo.INTEGER)
    public int id;

    @ColumnInfo(name = "name", typeAffinity = ColumnInfo.TEXT)
    public String name;

    /** --------------------------------------------------------------------
     * 第六章：Room 銷毀與重建
     * age 類型從 TEXT 改成 INT
     */
    @ColumnInfo(name = "age", typeAffinity = ColumnInfo.TEXT)
    public String age;
//    @ColumnInfo(name = "age", typeAffinity = ColumnInfo.INTEGER)
//    public int age;
    // ---------------------------------------------------------------------

    /** --------------------------------------------------------------------
     * 第六章：Room 銷毀與重建
     * age 類型從 TEXT 改成 INT
     */
    /**
     * Room 使用這個建構子操作數據
     * 用於 update
     */
    public Student(int id, String name, String age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
//    public Student(int id, String name, int age) {
//        this.id = id;
//        this.name = name;
//        this.age = age;
//    }
    // ---------------------------------------------------------------------

    /** --------------------------------------------------------------------
     * 第六章：Room 銷毀與重建
     * age 類型從 TEXT 改成 INT
     */
    /**
     * Room 只能識別一個建構子，若希望定義多個，需使用 Ignore 標籤，讓 Room 忽略這個建構子
     * 用於 insert
     */
    @Ignore
    public Student(String name, String age) {
        this.name = name;
        this.age = age;
    }
//    @Ignore
//    public Student(String name, int age) {
//        this.name = name;
//        this.age = age;
//    }
    // ---------------------------------------------------------------------


    /** --------------------------------------------------------------------
     * 第六章：Room 銷毀與重建
     * age 類型從 TEXT 改成 INT
     */
    /**
     * 第六章：Room 資料庫升級
     * 增加欄位
     */
    @ColumnInfo(name = "addField", typeAffinity = ColumnInfo.TEXT)
    public String addField;
    /**
     * Room 使用這個建構子操作數據
     * 用於 update
     */
    @Ignore
    public Student(int id, String name, String age, String addField) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.addField = addField;
    }
//    @Ignore
//    public Student(int id, String name, int age, String addField) {
//        this.id = id;
//        this.name = name;
//        this.age = age;
//        this.addField = addField;
//    }
    // -------------------------------------------------------

    /** --------------------------------------------------------------------
     * 第六章：Room 銷毀與重建
     * age 類型從 TEXT 改成 INT
     * // 如果欄位在 Student Table 已經存在，就不能新增這個欄位
     * // android.database.sqlite.SQLiteException: duplicate column name: addField_double
     */
//    @ColumnInfo(name = "addField_double", typeAffinity = ColumnInfo.TEXT)
//    public String addField_double;

}
