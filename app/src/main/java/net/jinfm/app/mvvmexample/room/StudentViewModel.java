package net.jinfm.app.mvvmexample.room;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

/**
 * 第六章：Room
 * Room 與 LiveData、ViewModel 結合使用
 */
public class StudentViewModel extends AndroidViewModel {

    private MyDatabase myDatabase;
    private LiveData<List<Student>> liveDataStudent;

    public StudentViewModel(@NonNull Application application) {
        super(application);
        myDatabase = MyDatabase.getInstance(application);
        liveDataStudent = myDatabase.studentDao().getStudentList_LiveData();
    }

    public LiveData<List<Student>> getLiveDataStudent() {
        return liveDataStudent;
    }
}
