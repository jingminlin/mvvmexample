package net.jinfm.app.mvvmexample.paging.itemkeydatasource;

import net.jinfm.app.mvvmexample.paging.itemkeydatasource.model.User;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

/**
 * 第九章：Paging
 * 通過 LivePageListBuilder 創建和配置 PagedList
 * 並使用 LiveData 包裝 PagedList 將其暴露給 MainActivity
 */
public class UserViewModel extends ViewModel {

    public LiveData<PagedList<User>> UserPagedList;

    public UserViewModel(){
        PagedList.Config config = (new PagedList.Config.Builder())
//                .setEnablePlaceholders(true) // 設置佔位符
                .setEnablePlaceholders(false) // 設置佔位符
                .setPageSize(UserDataSource.PER_PAGE) // 設置每頁大小，通常與 DataSource 中請求數據的參數保持一致
                .setPrefetchDistance(10) // 設置距離底部還有多少數據時開始加載下一頁

                // 設置首次加載數據的數量，須為 PageSize 的整數倍，預設為 3 倍
                .setInitialLoadSizeHint(UserDataSource.PER_PAGE * 4)
                // 設置能承受的最大數量，超出該值可能會出現異常
                .setMaxSize(65536 * UserDataSource.PER_PAGE)
                .build();
        UserPagedList = (new LivePagedListBuilder<>(
                new UserDataSourceFactory(), config)).build();
    }
}