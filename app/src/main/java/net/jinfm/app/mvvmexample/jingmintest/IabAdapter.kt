package net.jinfm.app.mvvmexample.jingmintest

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.adapter_iab.view.*
import net.jinfm.app.mvvmexample.R
import net.jinfm.app.mvvmexample.databinding.AdapterIabBinding
import net.jinfm.app.mvvmexample.jingmintest.model.ServerIABData

class IabAdapter constructor(products: List<ServerIABData.Product>, val clickListener: (ServerIABData.Product, Int) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var products: List<ServerIABData.Product>

    init {
        this.products = products
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val adapterIabBinding: AdapterIabBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.context), R.layout.adapter_iab, parent, false)
        return MyViewHolder(adapterIabBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val product = products[position]
        (holder as MyViewHolder).adapterIabBinding.product = product
        holder?.itemView.setOnClickListener { clickListener(product, position) }
        if (product.gift != null) {
            if (product.gift.size >= 1) {
                holder?.itemView.icon_tick1.visibility = View.VISIBLE
                holder?.itemView.tv_gift1.text = product.gift[0].giftName
            }
            if (product.gift.size >= 2) {
                holder?.itemView.icon_tick2.visibility = View.VISIBLE
                holder?.itemView.tv_gift2.text = product.gift[1].giftName
            }
        }
    }

    override fun getItemCount(): Int {
        return products.size
    }

    internal inner class MyViewHolder(itemView: AdapterIabBinding) : RecyclerView.ViewHolder(itemView.getRoot()) {
        var adapterIabBinding: AdapterIabBinding
        init {
            // TextView 設定刪除線有兩種方式
            // https://codertw.com/android-%E9%96%8B%E7%99%BC/331990/
//            itemView.tvOriginalprice.setPaintFlags(itemView.tvOriginalprice.getPaintFlags() or Paint.STRIKE_THRU_TEXT_FLAG)
//            itemView.tvOriginalprice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG)
//        holder?.itemView.tv_originalprice
//                .setPaintFlags(holder?.itemView.tv_originalprice.getPaintFlags() or Paint.STRIKE_THRU_TEXT_FLAG)
//            adapterIabBinding.tv_originalprice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG)
            adapterIabBinding = itemView
        }
    }

    // 動態新增 gift
    private fun addGift(position: Int) {

    }
}