package net.jinfm.app.mvvmexample.paging.itemkeydemo.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import net.jinfm.app.mvvmexample.paging.itemkeydemo.repository.ItemRepo

class ItemKeyDemoViewModel : ViewModel() {

    private val itemRepo = ItemRepo()

    fun getItems(): LiveData<PagedList<Item>> {
        return itemRepo.getItems()
    }
}