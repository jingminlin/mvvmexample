package net.jinfm.app.mvvmexample.databindingex;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewViewModel {
    public List<BookModel> getBooks() {
        List<BookModel> bookModels = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            BookModel bookModel = new BookModel("Android Jetpack 應用指南 " + i, "葉問 " + i);
            bookModel.image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7uZ1qJxnO65vIEoEKjWxCUfjd-Xe5SqH8Zw&usqp=CAU";
                    bookModels.add(bookModel);
        }
        return bookModels;
    }
}
