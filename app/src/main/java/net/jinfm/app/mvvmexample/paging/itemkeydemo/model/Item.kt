package net.jinfm.app.mvvmexample.paging.itemkeydemo.model

data class Item(val id: Int, val name: String)