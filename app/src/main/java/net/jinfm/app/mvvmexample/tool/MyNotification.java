package net.jinfm.app.mvvmexample.tool;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import net.jinfm.app.mvvmexample.R;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.navigation.Navigation;

public class MyNotification {

    /**
     * 第三章 Navigation
     * 深層連結 DeepLink
     * PendingIntent 方式
     */

    Context mContext;
    String CHANNEL_ID = "20210219";
    int notificationId = 20210219;

    public MyNotification(Context context) {
        this.mContext = context;
    }

    public void sendNotification() {
        if (mContext == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "ChannelName", importance);
            channel.setDescription("description");
            NotificationManager notificationManager = mContext.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder builder = new NotificationCompat
                .Builder(mContext, CHANNEL_ID)
                .setSmallIcon(R.drawable.imgpsh_fullsize)
                .setContentTitle("DeepLinkDemo")
                .setContentText("Hello World!")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(getPendingIntent())
                .setAutoCancel(true);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(mContext);
        notificationManager.notify(notificationId, builder.build());
    }

    /**
     * 通過 PendingIntent 設置當通知被單擊時需要跳轉到的 destination 以及傳遞的參數
     *
     */
    private PendingIntent getPendingIntent() {
        if (mContext != null) {
            Bundle bundle = new Bundle();
            bundle.putString("params", "ParamsFromNotification_HelloMichael");
            return Navigation.findNavController((Activity) mContext, R.id.nav_host_fragment)
                    .createDeepLink()
                    .setGraph(R.navigation.nav_graph)
                    .setDestination(R.id.secondFragment)
                    .setArguments(bundle)
                    .createPendingIntent();
        }
        return null;
    }


}
