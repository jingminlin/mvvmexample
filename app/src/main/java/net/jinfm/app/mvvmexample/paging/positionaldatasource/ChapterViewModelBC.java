package net.jinfm.app.mvvmexample.paging.positionaldatasource;

import android.app.Application;
import android.os.AsyncTask;

import net.jinfm.app.mvvmexample.paging.positionaldatasource.db.ChapterDatabase;
import net.jinfm.app.mvvmexample.paging.positionaldatasource.model.Chapter;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

/**
 * Room 組件對 Paging 組件提供了原生支援
 * 因此 LivePagedListBuilder 在創建 PagedList 時，可以直接將 Room 作為數據原
 * 再通過 setBoundaryCallback() 將 PageList 與 BoundaryCallback 關聯起來
 * 需要注意的是，由於數據庫需要用到 Context 因此 ViewModel 需要繼承自 AndroidViewModel，也因此 ChapterViewModel 的生命週期與 Application 及數據庫的生命週期是一樣的
 * 當然如果數據庫是在 Application 中進行初始化就沒有這個問題了，取決於架構設計
 */
public class ChapterViewModelBC extends AndroidViewModel {

    public LiveData<PagedList<Chapter>> chapterPagedList;

    public ChapterViewModelBC(Application application){
        super(application);
        ChapterDatabase database = ChapterDatabase.getInstance(application);
        chapterPagedList = (new LivePagedListBuilder<>(
                database.chapterDao().getChapterList(), ChapterBoundaryCallback.amount_per_page))
                .setBoundaryCallback(new ChapterBoundaryCallback(application))
                .build();
    }
    /**
     * 更新數據
     */
    public void refresh(){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                ChapterDatabase.getInstance(getApplication())
                        .chapterDao()
                        .clear();
            }
        });
    }
}
