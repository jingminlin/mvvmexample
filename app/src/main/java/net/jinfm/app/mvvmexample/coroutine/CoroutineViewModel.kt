package net.jinfm.app.mvvmexample.coroutine

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*

class CoroutineViewModel : ViewModel() {

    val coroutineScope = CoroutineScope(Dispatchers.Main + Job() + errorHandler)

    override fun onCleared() {
        coroutineScope.cancel()
        super.onCleared()
    }

    companion object {
        /**
         * 這個 errorHandler 會接到這個 CoroutineScope 沒有被 try-catch 包起來的 exceptions
         * 如果少了它，exceptions 會跑到 App 預設的 Thread.UncaughtExceptionHandler
         * 使用者最後就會看到 App crash 的對話框。為了避免慘劇發生，建議在每個 CoroutineScope 都放上 CoroutineExceptionHandler
         */
        val errorHandler = CoroutineExceptionHandler { _, error ->
            error.printStackTrace()
//            TrackingCrash.logException(error)
        }
    }

    // ----------------------------------------------------------------

    // val job1 = async { loadA() } 這邊的 loadA() 會在 job1 回傳後開始讀取資料，然後程式會接續執行到 job1.await() ， job1.await() 會等待 loadA() 讀完資料後才繼續往下執行。
    // 1. loadA() 開始讀取資料
    // 2. loadB() 開始讀取資料
    // 3. job1.await() 等待 loadA() 完成讀取
    // 4. job2.await() 等待 loadB() 完成讀取
    // Kotlin 透過 suspend 這個關鍵字來完成 callback 流程的簡化，await() 本身就是一個 suspend function，它會在裡面的非同步任務完成時，自動 callback 回來，開發者就不用再寫 callback listener 了。
//    fun load() {
//        coroutineScope.launch(Dispatchers.IO) {
//            val job1 = async { loadA() }
//            val job2 = async { loadB() }
//
//            // suspend 這個關鍵字來完成 callback 流程的簡化，await() 本身就是一個 suspend function
//            // 它會在裡面的非同步任務完成時，自動 callback 回來，開發者就不用再寫 callback listener 了
//            val list1 = job1.await()
//            val list2 = job2.await()
//        }
//    }

    // ----------------------------------------------------------------

    // loadA() 把 retrofit 的 callback listener 包裝成 suspend function，讓 load() 不需要寫 callback 就能使用。
    //suspendCoroutine 會傳入一個 continuation ，當呼叫 continuation.resume() 時，
    // loadA() 的 suspend 就會結束並將 list 傳給 list1 ；當呼叫 continuation.resumeWithException(error) ，
    // loadA() 的 suspend 也會結束並拋出 exception error
//    fun load() {
//        coroutineScope.launch {
//            val list1 = loadA()
//        }
//    }
//    private suspend fun loadA() = suspendCoroutine<List<Data>> { continuation ->
//        val call = api.loadA()
//        call.enqueue(object: Callback<List<Data>> {
//            override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
//                val list = response.body()
//                if (list != null) {
//                    continuation.resume(list)
//                } else {
//                    continuation.resumeWithException(IllegalStateException("list is null"))
//                }
//            }
//
//            override fun onFailure(call: Call<List<Data>>, error: Throwable) {
//                continuation.resumeWithException(error)
//            }
//        })
//    }

    // ----------------------------------------------------------------

    // suspendCancellableCoroutine

    // suspendCancellableCoroutine 和 suspendCoroutine 的差異在於，
    // 前者的 continuation 可以設定被取消時的 callback，通常會希望 App 離開時停止所有的 coroutines
    // 也一併停止所有的 retrofit requests，這個 cancel callback 就非常有用
    // continuation.invokeOnCancellation{} 指定了這個 coroutine 被 cancel 時要做的事，就是call.cancel() 把 retrofit 的 request 取消
//    private suspend fun loadA() = suspendCancellableCoroutine<List<Data>> { continuation ->
//        val call = api.loadA()
//        call.enqueue(object: Callback<List<Data>> {
//            // ...
//        })
//        continuation.invokeOnCancellation {
//            call.cancel()
//        }
//    }


}