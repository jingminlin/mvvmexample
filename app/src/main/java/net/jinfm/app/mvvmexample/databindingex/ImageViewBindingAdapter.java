package net.jinfm.app.mvvmexample.databindingex;

import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import net.jinfm.app.mvvmexample.R;

import androidx.databinding.BindingAdapter;

/**
 * 第八章：DataBinding
 * 自定義 BindingAdapter
 */
public class ImageViewBindingAdapter {

    private static final String TAG = "ImageViewBindingAdapter";

    // 定義別名 image
    // 自定義 BindingAdapter 網路圖片
    @BindingAdapter("image")
    public static void setImage(ImageView imageView, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.get()
                    .load(imageUrl)
                    .placeholder(R.drawable.imgpsh_fullsize)
                    .error(R.drawable.imgpsh_fullsize)
                    .into(imageView);
        } else {
            imageView.setBackgroundColor(Color.DKGRAY);
        }
    }

    // 定義別名 localImage
    // 自定義 BindingAdapter 本地圖片
    @BindingAdapter("localImage")
    public static void setLocalImage(ImageView imageView, int imageResource) {
        if (!TextUtils.isEmpty(imageResource + "")) {
            imageView.setImageResource(imageResource);
        } else {
            imageView.setBackgroundColor(Color.DKGRAY);
        }
    }

    // 定義別名 localImage
    // 自定義 BindingAdapter 多參數重載
    // requireAll 告訴 DataBinding 這些參數是否都要有值，預設為 true
    @BindingAdapter(value = {"image", "localImage"}, requireAll = false)
    public static void setImage(ImageView imageView, String imageUrl, int imageResource) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.get()
                    .load(imageUrl)
                    .placeholder(R.drawable.imgpsh_fullsize)
                    .error(R.drawable.imgpsh_fullsize)
                    .into(imageView);
        } else {
            imageView.setImageResource(imageResource);
        }
    }

    // 可選舊值
    // 得到修改前的值，防止方斐重複調用
    @BindingAdapter("padding")
    public static void setPadding(View view, int oldPadding, int newPadding) {
        Log.e(TAG , "第八章:可選舊值 oldPadding: " + oldPadding + " / newPadding: " + newPadding);
        if (oldPadding != newPadding) {
            view.setPadding(newPadding, newPadding, newPadding, newPadding);
        }
    }


}
