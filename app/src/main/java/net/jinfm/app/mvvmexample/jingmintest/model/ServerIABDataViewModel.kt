package net.jinfm.app.mvvmexample.jingmintest.model

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import net.jinfm.app.mvvmexample.jingmintest.network.IabPageApi

class ServerIABDataViewModel constructor(reload: Boolean = true) : ViewModel() {

    /**
     * Server IAB 商品資料
     */
    companion object {
        private const val TAG = "ServerIABDataViewModel"
    }

    private val _ServerIabData = MutableLiveData<ServerIABData>()
    val ServerIabData: LiveData<ServerIABData>
        get() = _ServerIabData

    private val _status = MutableLiveData<LoadApiStatus>()
    val status: LiveData<LoadApiStatus>
        get() = _status

    private val _error = MutableLiveData<String>()
    val error: LiveData<String>
        get() = _error

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    init {
//        getServerIabDataResult(true)
        getServerIabDataResult(reload)
//        getServerIabDataResult_string(true)
    }

    private fun getServerIabDataResult(isInitial: Boolean = false) {
        coroutineScope.launch {
            val result = getServerIabData()
            Log.d(TAG, "result: $result")

            _ServerIabData.value = when (result) {
                is Result.Success -> {
                    _error.value = null
                    if (isInitial) _status.value = LoadApiStatus.DONE
                    result.data
                }
                is Result.Fail -> {
                    Log.d(TAG, "${result.error} ")
                    _error.value = result.error.toString()
                    if (isInitial) _status.value = LoadApiStatus.ERROR
                    null
                }
                is Result.Error -> {
                    Log.d(TAG, "$result. ")
                    _error.value = result.exception.toString()
                    if (isInitial) _status.value = LoadApiStatus.ERROR
                    null
                }
                else -> {
                    if (isInitial) _status.value = LoadApiStatus.ERROR
                    null
                }
            }
        }
    }

    // suspend 可以被暫停
    // suspend 必須要在 coroutineScope 的 launch 、runBlocking 內或者是其他的 suspend method 才能夠被使用
    suspend fun getServerIabData(): Result<ServerIABData> {
        return try {
            val ServerIabDataResult = IabPageApi.retrofitService.getServerIabData()

            Result.Success(ServerIabDataResult)

        } catch (e: Exception) {
            Log.d(TAG, "${this::class.simpleName} exception=${e.message}")
            Result.Error(e)
        }
    }

    // -------------------------------------------------------------

    private val _ServerIabData_string = MutableLiveData<String>()
    val ServerIabData_string: LiveData<String>
        get() = _ServerIabData_string

    private fun getServerIabDataResult_string(isInitial: Boolean = false) {
        coroutineScope.launch {
            val result = getServerIabData_string()
            Log.d(TAG, "getServerIabDataResult result: $result")

            _ServerIabData_string.value = when (result) {
                is Result.Success -> {
                    _error.value = null
                    if (isInitial) _status.value = LoadApiStatus.DONE
                    result.data
                }
                is Result.Fail -> {
                    Log.d(TAG, "${result.error} ")
                    _error.value = result.error.toString()
                    if (isInitial) _status.value = LoadApiStatus.ERROR
                    null
                }
                is Result.Error -> {
                    Log.d(TAG, "$result. ")
                    _error.value = result.exception.toString()
                    if (isInitial) _status.value = LoadApiStatus.ERROR
                    null
                }
                else -> {
                    if (isInitial) _status.value = LoadApiStatus.ERROR
                    null
                }
            }
        }
    }

    // suspend 可以被暫停
    // suspend 必須要在 coroutineScope 的 launch 、runBlocking 內或者是其他的 suspend method 才能夠被使用
    suspend fun getServerIabData_string(): Result<String> {
        return try {
            val ServerIabDataResult = IabPageApi.retrofitService.getServerIabData_string()

            Result.Success(ServerIabDataResult)

        } catch (e: Exception) {
            Log.d(TAG, "${this::class.simpleName} exception=${e.message}")
            Result.Error(e)
        }
    }

}
