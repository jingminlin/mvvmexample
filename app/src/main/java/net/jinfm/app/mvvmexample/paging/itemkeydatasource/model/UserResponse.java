package net.jinfm.app.mvvmexample.paging.itemkeydatasource.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserResponse {

    @SerializedName("items")
    public List<User> users;

}
