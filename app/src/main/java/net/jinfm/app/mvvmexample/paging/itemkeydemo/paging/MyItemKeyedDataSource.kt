package net.jinfm.app.mvvmexample.paging.itemkeydemo.paging

import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import androidx.paging.ItemKeyedDataSource
import net.jinfm.app.mvvmexample.paging.itemkeydemo.data.ItemData
import net.jinfm.app.mvvmexample.paging.itemkeydemo.model.Item

/**
 * 使用時機：
 * 當 RecyclerView 資料的 Key 有連續性，可根據資料的 Key 找到上一筆或下一筆資料
 */

class MyItemKeyedDataSource() : ItemKeyedDataSource<Int, Item>(), Parcelable {

    private val tag = this::class.java.simpleName

    constructor(parcel: Parcel) : this() {
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Item>) {

        // Key -> 查詢的索引
        // Size -> 要求資料的長度

        val initKey = params.requestedInitialKey ?: 0
        val size = params.requestedLoadSize

        Log.i(tag, "loadInitial -> key: $initKey, size: $size")

        callback.onResult(ItemData.getIncreaseItems(initKey, size))
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Item>) {

        // Key -> 查詢的索引，這裡給的是上一頁最後一筆資料的Key
        // Size -> 要求資料的長度

        val previousLatestKey = params.key
        val size = params.requestedLoadSize

        Log.i(tag, "loadAfter -> key: $previousLatestKey, size: $size")

        val newKey = previousLatestKey + 1

        callback.onResult(ItemData.getIncreaseItems(newKey, size))
    }


    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Item>) {

        // Key -> 查詢的索引，這裡給的是上一頁第一筆資料的Key
        // Size -> 要求資料的長度

        val previousFirstKey = params.key
        val size = params.requestedLoadSize

        Log.i(tag, "loadBefore -> key: $previousFirstKey, size: $size")

        callback.onResult(ItemData.getReduceItems(previousFirstKey, size))
    }

    override fun getKey(item: Item): Int = item.id
    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MyItemKeyedDataSource> {
        override fun createFromParcel(parcel: Parcel): MyItemKeyedDataSource {
            return MyItemKeyedDataSource(parcel)
        }

        override fun newArray(size: Int): Array<MyItemKeyedDataSource?> {
            return arrayOfNulls(size)
        }
    }
}