package net.jinfm.app.mvvmexample;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import net.jinfm.app.mvvmexample.room.MyDatabase;
import net.jinfm.app.mvvmexample.room.QueryStudentTask_;
import net.jinfm.app.mvvmexample.room.QueryStudentTask_LiveData;
import net.jinfm.app.mvvmexample.room.Student;
import net.jinfm.app.mvvmexample.tool.TimerViewModel;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;

public class roomFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = this.getClass().getName();
    private View view;
    private Context context;
    private Activity activity;
    private roomFragment roomFragment;

    private final int ROOM = 0;
    private MyDatabase myDatabase;
    private Boolean isLiveData;
    private List<Student> studentList;
    private int timeStep = 0;


    public roomFragmentAdapter(Context context, Activity activity, roomFragment roomFragment, MyDatabase myDatabase, Boolean isLiveData) {
        this.studentList = null;
        this.context = context;
        this.activity = activity;
        this.roomFragment = roomFragment;
        this.myDatabase = myDatabase;
        this.isLiveData = isLiveData;

        init(); // init timeStep

    }

    public roomFragmentAdapter(Context context, Activity activity, roomFragment roomFragment, Boolean isLiveData) {
        this.studentList = null;
        this.context = context;
        this.activity = activity;
        this.roomFragment = roomFragment;
//        this.myDatabase = myDatabase;
        this.isLiveData = isLiveData;

        init(); // init timeStep

    }

    public void roomFragmentAdapter(List<Student> studentList) {
        this.studentList = studentList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == ROOM) {

            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_roomfragment, parent, false);

            Holder holder = new Holder(view);

            return holder;

        }

        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof Holder) {

            ((Holder) holder).id.setText(studentList.get(position).id + "");
            ((Holder) holder).name.setText(studentList.get(position).name);
            ((Holder) holder).age.setText(studentList.get(position).age + " / " + studentList.get(position).addField);

            ((Holder) holder).update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isLiveData) {
                        /**
                         * 第六章：Room
                         * Room 與 LiveData、ViewModel 結合使用
                         */
                        QueryStudentTask_LiveData queryStudentTask_LiveData = new QueryStudentTask_LiveData(context);
                        queryStudentTask_LiveData.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "update", studentList.get(position).id + "", "更新 " + timeStep , timeStep + "");
                    } else {
                        QueryStudentTask_ queryStudentTask_ = new QueryStudentTask_(context);
                        queryStudentTask_.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "update", studentList.get(position).id + "", "更新 " + timeStep , timeStep + "");
                        queryStudentTask_.setResultListener(new QueryStudentTask_.ResultListener() {
                            @Override
                            public void onFinished(Boolean result, String status, List<Student> studentList) {
                                if (result && status.equals("200")) {
                                    roomFragmentAdapter(studentList);
                                }
                            }
                        });
                    }
                }
            });


            ((Holder) holder).delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isLiveData) {
                        /**
                         * 第六章：Room
                         * Room 與 LiveData、ViewModel 結合使用
                         */
                        QueryStudentTask_LiveData queryStudentTask_LiveData = new QueryStudentTask_LiveData(context);
                        queryStudentTask_LiveData.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "delete", studentList.get(position).id + "");
                    } else {
                        QueryStudentTask_ queryStudentTask_ = new QueryStudentTask_(context);
                        queryStudentTask_.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "delete", studentList.get(position).id + "");
                        queryStudentTask_.setResultListener(new QueryStudentTask_.ResultListener() {
                            @Override
                            public void onFinished(Boolean result, String status, List<Student> studentList) {
                                if (result && status.equals("200")) {
                                    roomFragmentAdapter(studentList);
                                }
                            }
                        });
                    }
                }
            });

        }


    }

    @Override
    public int getItemCount() {

        if (studentList == null) {
            return 0;
        }
        return studentList.size();

    }

    @Override
    public int getItemViewType(int position) {

        return ROOM;

    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    class Holder extends RecyclerView.ViewHolder {

        TextView id;
        TextView name;
        TextView age;
        Button update;
        Button delete;

        public Holder(View itemView) {
            super(itemView);

            id = (TextView) itemView.findViewById(R.id.id);
            name = (TextView) itemView.findViewById(R.id.name);
            age = (TextView) itemView.findViewById(R.id.age);
            update = (Button) itemView.findViewById(R.id.update);
            delete = (Button) itemView.findViewById(R.id.delete);

        }
    }

    private void init() {

        // 第四章：ViewModel ---------
//        final TextView tvTime_LiveData = view.findViewById(R.id.tvTime_LiveData);
        // 通過 ViewModelProvider 得到 ViewModel
        // 注意：這裡的參數需要的是 Activity，而不是 Fragment，否則將收不到監聽
//        TimerViewModel timerViewModel_LiveData = new ViewModelProvider(this).get(TimerViewModel.class);
        TimerViewModel timerViewModel_LiveData = new ViewModelProvider((ViewModelStoreOwner) activity).get(TimerViewModel.class);
        // 得到 ViewModel 中的 LiveData
        final MutableLiveData<Integer> liveData = (MutableLiveData<Integer>) timerViewModel_LiveData.getCurrentSecond();
        // 方法一：通過 LiveData.observe() 觀察 ViewModel 中的數據變化
        liveData.observe(roomFragment.getViewLifecycleOwner(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                // 收到回調後更新 UI 介面
//                tvTime_LiveData.setText("Time :" + integer);
                timeStep = integer;
                // 只有在 Lifecycle.State.ON_STARTED 或 Lifecycle.State.ON_RESUME 時，頁面才能收到來自 LiveDate 的通知
                Log.d(TAG, "observe" + integer);
            }
        });
        // 方法二：通過 LiveData.observeForever() 觀察 ViewModel 中的數據變化
//        liveData.observeForever(new Observer<Integer>() {
//            @Override
//            public void onChanged(@Nullable Integer integer) {
//                tvTime_LiveData.setText("Time :" + integer);
//                // Lifecycle.State.ON_STARTED 或 Lifecycle.State.ON_RESUME 和其他狀態，頁面都能收到來自 LiveDate 的通知
//                Log.d(TAG, "observeForever" + integer);
////                liveData.removeObserver(this);
//            }
//        });
//        // 前一頁已啟動過了
//        timerViewModel_LiveData.startTiming();
        // ---------------------

    }


}
