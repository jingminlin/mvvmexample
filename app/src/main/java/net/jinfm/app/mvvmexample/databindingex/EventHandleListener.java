package net.jinfm.app.mvvmexample.databindingex;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import net.jinfm.app.mvvmexample.databinding.FragmentDatabindingBinding;

public class EventHandleListener {
    private Context context;
    private FragmentDatabindingBinding dataBinding;

    public EventHandleListener(Context context, FragmentDatabindingBinding dataBinding) {
        this.context = context;
        this.dataBinding = dataBinding;
    }

    public void onButtonClicked(View view) {
        Toast.makeText(context, "I am clicked ", Toast.LENGTH_SHORT).show();
    }

    public void onButtonClicked(View view, int rating) {
        Toast.makeText(context, "I am clicked " + rating, Toast.LENGTH_SHORT).show();
    }

    public void onImageClicked(View view) {
        dataBinding.setNetworkImage(null);
    }

    // 可選舊值
    public void onClickedChangeImagePadding(View view) {
        dataBinding.setImagePadding(180);
    }

    public void onClickedSetEditText(View view) {
        dataBinding.getLoginModel().userName.set("test text");
    }
}
