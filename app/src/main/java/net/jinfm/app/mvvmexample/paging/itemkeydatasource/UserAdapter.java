package net.jinfm.app.mvvmexample.paging.itemkeydatasource;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.jinfm.app.mvvmexample.R;
import net.jinfm.app.mvvmexample.paging.itemkeydatasource.model.User;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 第九章：Paging
 */
public class UserAdapter extends PagedListAdapter<User, UserAdapter.UserViewHolder> {

    private Context context;

    public UserAdapter(Context context){
        super(DIFF_CALLBACK);
        this.context = context;
    }

    /**
     * DiffUtil 工具介紹
     * 計算兩個數據列之間的差異
     * 以前的做法是 notifyDataSetChanged() 對整個數據源進行刷新，效率不高，且增刪的動畫效果較困難
     * DoffUtil 只會更新需要更新的數據，不需要刷新整個數據源，且可輕鬆地加入動畫效果
     *
     */
    private static final DiffUtil.ItemCallback<User> DIFF_CALLBACK = new DiffUtil.ItemCallback<User>() {
        @Override
        public boolean areItemsTheSame(@NonNull User oldItem, @NonNull User newItem) {
            return false;
        }

        @Override
        public boolean areContentsTheSame(@NonNull User oldItem, @NonNull User newItem) {
            return false;
        }
    };

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_item, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        /**
         * 若有數據，直接將數據與 UI 綁定
         * 若沒有數據，則 getItem() 會通知 PageList 去獲取下一頁的數據，PagedList 收到通知後，讓 DataSource 執行具體的數據獲取工作
         */
        User user = getItem(position);
        if (user != null){
            Picasso.get().load(user.getAvatar_url()).fit().into(holder.profile_image);
            holder.position.setText(position + "");
            holder.account_id.setText(user.getId() + "");
            holder.display_name.setText(user.getLogin());
        } else {
            holder.position.setText(position + "");
            holder.account_id.setText("空的 user");
            holder.display_name.setText("空的 title");
        }
    }


    class UserViewHolder extends RecyclerView.ViewHolder{
        ImageView profile_image;
        TextView position;
        TextView account_id;
        TextView display_name;
        public UserViewHolder(View itemView) {
            super(itemView);
            profile_image = itemView.findViewById(R.id.profile_image);
            position = itemView.findViewById(R.id.position);
            account_id = itemView.findViewById(R.id.account_id);
            display_name = itemView.findViewById(R.id.display_name);
        }
    }
}
