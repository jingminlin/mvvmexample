package net.jinfm.app.mvvmexample.paging.pagekeyeddatasource.model;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("account_id")
    public int id;

    @SerializedName("display_name")
    public String name;

    @SerializedName("profile_image")
    public String avatar;

    public User(int id, String name, String avatar){
        this.id = id;
        this.name =name;
        this.avatar = avatar;
    }
}
