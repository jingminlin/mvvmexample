package net.jinfm.app.mvvmexample.room;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

public class QueryStudentTask_ extends AsyncTask<String, Void, Boolean> {

    private Context mContext;
    private String status = "200";

    private String action = "";
    private int id = 0;
    private String name = "";
    /**
     * 第六章：Room 銷毀與重建
     * age 類型從 TEXT 改成 INT
     */
    private String age = "";
//    private int age = 0;

    private QueryStudentTask_.ResultListener mListener;
    private MyDatabase myDatabase;
    private List<Student> studentList;

    public QueryStudentTask_(Context mContext) {
        this.mContext = mContext;
    }

    public void setResultListener(QueryStudentTask_.ResultListener mListener) {
        this.mListener = mListener;
    }

    protected Boolean doInBackground(String... params) {
        try {

            action = params[0];

            if (action == null) {
                return false;
            } else {

                this.myDatabase = MyDatabase.getInstance(mContext);

                switch (action) {
                    case "init":
                        if (myDatabase.studentDao().getStudentList().size() == 0) {
//                            myDatabase.studentDao().deleteStudentAll();
                            myDatabase.studentDao().insertStudent(new Student("A", "11"));
                            myDatabase.studentDao().insertStudent(new Student("B", "12"));
                            myDatabase.studentDao().insertStudent(new Student("C", "13"));
                        }
                        studentList = myDatabase.studentDao().getStudentList();
                        break;
                    case "insert":
                        id = 0;
                        name = params[1];
                        age = params[2];
//                        age = Integer.parseInt(params[2]);
                        myDatabase.studentDao().insertStudent(new Student(name, age));
                        studentList = myDatabase.studentDao().getStudentList();
                        break;
                    case "update":
                        id = Integer.parseInt(params[1]);
                        name = params[2];
                        age = params[3];
//                        age = Integer.parseInt(params[3]);
                        myDatabase.studentDao().updateStudent(new Student(id, name, age));
                        studentList = myDatabase.studentDao().getStudentList();
                        break;
                    case "delete":
                        id = Integer.parseInt(params[1]);
                        name = "";
                        age = "";
//                        age = 0;
                        myDatabase.studentDao().deleteStudentById(id);
                        studentList = myDatabase.studentDao().getStudentList();
                        break;
                }
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (this.mListener != null) {
            this.mListener.onFinished(result, this.status, this.studentList);
        }

    }

    public interface ResultListener {
        void onFinished(Boolean result, String status, List<Student> studentList);
    }
}