package net.jinfm.app.mvvmexample.paging.itemkeydatasource;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.jinfm.app.mvvmexample.R;
import net.jinfm.app.mvvmexample.paging.itemkeydatasource.model.User;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ItemKeyFragment extends Fragment {

    private String TAG = this.getClass().getName();
    private View view;
    private RecyclerView recycler_view;
    private UserAdapter adapter;
    private UserViewModel userViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_itemkey, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        init();

    }

    private void init() {

        recycler_view = view.findViewById(R.id.recycler_view);
        recycler_view.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler_view.setHasFixedSize(true);

        // 此方法抓的 API 會抓不到資料
        adapter = new UserAdapter(getContext());
        userViewModel = new ViewModelProvider(getActivity()).get(UserViewModel.class);
        userViewModel.UserPagedList.observe(getActivity(), new Observer<PagedList<User>>() {
            @Override
            public void onChanged(PagedList<User> users) {
                adapter.submitList(users);
            }
        });
        recycler_view.setAdapter(adapter);

    }
}
