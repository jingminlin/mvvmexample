package net.jinfm.app.mvvmexample.tool;

import androidx.lifecycle.LifecycleService;

/**
 * 第二章：LifeCycle
 */
public class MyService extends LifecycleService {

    // 由於 LifecycleService 是 Service 的直接子類，所以與普通 Service 沒有差別

    private MyServiceObserver myServiceObserver;

    public MyService () {

        myServiceObserver = new MyServiceObserver();

        // 將觀察者與被觀察者綁定
        getLifecycle().addObserver(myServiceObserver);

    }

}
