package net.jinfm.app.mvvmexample.paging.positionaldatasource;

import net.jinfm.app.mvvmexample.paging.positionaldatasource.model.Chapter;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

/**
 * 第九章：Paging
 * 通過 LivePageListBuilder 創建和配置 PagedList
 * 並使用 LiveData 包裝 PagedList 將其暴露給 MainActivity
 */
public class ChapterViewModel extends ViewModel {
    public LiveData<PagedList<Chapter>> chapterPagedList;
    public ChapterViewModel(){
        PagedList.Config config = (new PagedList.Config.Builder())
//                .setEnablePlaceholders(true) // 設置佔位符
                .setEnablePlaceholders(false) // 設置佔位符
                .setPageSize(ChapterDataSource.amount_per_page) // 設置每頁大小，通常與 DataSource 中請求數據的參數保持一致
                .setPrefetchDistance(4) // 設置距離底部還有多少數據時開始加載下一頁

                // 設置首次加載數據的數量，須為 PageSize 的整數倍，預設為 3 倍
                .setInitialLoadSizeHint(ChapterDataSource.amount_per_page * 4)
                // 設置能承受的最大數量，超出該值可能會出現異常
                .setMaxSize(65536 * ChapterDataSource.amount_per_page)
                .build();
        chapterPagedList = (new LivePagedListBuilder<>(
                new ChapterDataSourceFactory(), config)).build();
    }
}

///**
// * Room 組件對 Paging 組件提供了原生支援
// * 因此 LivePagedListBuilder 在創建 PagedList 時，可以直接將 Room 作為數據原
// * 再通過 setBoundaryCallback() 將 PageList 與 BoundaryCallback 關聯起來
// * 需要注意的是，由於數據庫需要用到 Context 因此 ViewModel 需要繼承自 AndroidViewModel，也因此 ChapterViewModel 的生命週期與 Application 及數據庫的生命週期是一樣的
// * 當然如果數據庫是在 Application 中進行初始化就沒有這個問題了，取決於架構設計
// */
//public class ChapterViewModel extends AndroidViewModel {
//
//    public LiveData<PagedList<Chapter>> chapterPagedList;
//
//    public ChapterViewModel(Application application){
//        super(application);
//        ChapterDatabase database = ChapterDatabase.getInstance(application);
//        chapterPagedList = (new LivePagedListBuilder<>(
//                database.chapterDao().getChapterList(), ChapterBoundaryCallback.amount_per_page))
//                .setBoundaryCallback(new ChapterBoundaryCallback(application))
//                .build();
//    }
//    /**
//     * 更新數據
//     */
//    public void refresh(){
//        AsyncTask.execute(new Runnable() {
//            @Override
//            public void run() {
//                ChapterDatabase.getInstance(getApplication())
//                        .chapterDao()
//                        .clear();
//            }
//        });
//    }
//}
