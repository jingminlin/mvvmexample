package net.jinfm.app.mvvmexample.paging.testurl.api;


import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TestRetrofitClient {

//    private static final String BASE_URL = "https://story.udn.com/dcstore/";
    private static final String BASE_URL = "https://api.github.com/";
    private static final String API_KEY = "************";
    private static TestRetrofitClient retrofitClient;
    private Retrofit retrofit;

    private TestRetrofitClient() {
        retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
    }

    public static synchronized TestRetrofitClient getInstance(){
        if (retrofitClient == null){
            retrofitClient = new TestRetrofitClient();
        }
        return retrofitClient;
    }

    public TestApi getApi(){
        return retrofit.create(TestApi.class);
    }

    /**
     * 為每個請求添加 API_KEY 參數
     */
    private OkHttpClient getClient() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @NotNull
            @Override
            public Response intercept(@NotNull Interceptor.Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        .addQueryParameter("apikey", API_KEY)
                        .build();

                Request.Builder requestBuilder = original.newBuilder().url(url);
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        return httpClient.build();
    }

}
