package net.jinfm.app.mvvmexample.testUnit

import net.jinfm.app.mvvmexample.Totaller
import org.junit.Assert.assertEquals
import org.junit.Test

@Suppress("IllegalIdentifier")
//@file:Suppress("IllegalIdentifier")
class TotallerTest {
    @Test
    fun `should be able to add 3 and 4 - and it mustn't go wrong`() {
        val totaller = Totaller() // 建立物件

        // 測試是用動作與斷言組成的
        // 動作是一段做事情的程式
        // assertEquals 斷言。是一段檢查事情的程式
        // 確定兩個值是不是相等，如果不相等，會丟出例外，且測試失敗
        assertEquals(3, totaller.add(3)) // 確認當我們加 3 時，回傳值是 3
        assertEquals(7, totaller.add(4)) // 確認當我們加 4 時，回傳值是 7
        assertEquals(7, totaller.total) // 確認回傳值符合 total 變數的值
    }
}