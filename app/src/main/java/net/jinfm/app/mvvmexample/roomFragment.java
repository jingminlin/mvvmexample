package net.jinfm.app.mvvmexample;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.jinfm.app.mvvmexample.room.MyDatabase;
import net.jinfm.app.mvvmexample.room.QueryStudentTask_;
import net.jinfm.app.mvvmexample.room.QueryStudentTask_LiveData;
import net.jinfm.app.mvvmexample.room.Student;
import net.jinfm.app.mvvmexample.room.StudentViewModel;
import net.jinfm.app.mvvmexample.tool.TimerViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 第六章：Room
 */
public class roomFragment extends Fragment {

    private String TAG = this.getClass().getName();
    private View view;
    private static Context mContext;

    private RecyclerView recyclerView;
    private roomFragmentAdapter roomFragmentAdapter;
    private MyDatabase myDatabase;
    private int timeStep = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_room, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        init_timeStep();

        /**
         * 第六章：Room
         * Room 與 LiveData、ViewModel 結合使用
         */
        initRoomCallBack();
//        initRoomLiveData();

    }

    private void init_timeStep() {

        // 第四章：ViewModel ---------
        final TextView tvTime_LiveData = view.findViewById(R.id.tvTime_LiveData);
        // 通過 ViewModelProvider 得到 ViewModel
        // 注意：這裡的參數需要的是 Activity，而不是 Fragment，否則將收不到監聽
//        TimerViewModel timerViewModel_LiveData = new ViewModelProvider(this).get(TimerViewModel.class);
        TimerViewModel timerViewModel_LiveData = new ViewModelProvider(this.getActivity()).get(TimerViewModel.class);
        // 得到 ViewModel 中的 LiveData
        final MutableLiveData<Integer> liveData = (MutableLiveData<Integer>) timerViewModel_LiveData.getCurrentSecond();
        // 方法一：通過 LiveData.observe() 觀察 ViewModel 中的數據變化
        liveData.observe(getViewLifecycleOwner(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                // 收到回調後更新 UI 介面
                tvTime_LiveData.setText("Time :" + integer);
                timeStep = integer;
                // 只有在 Lifecycle.State.ON_STARTED 或 Lifecycle.State.ON_RESUME 時，頁面才能收到來自 LiveDate 的通知
                Log.d(TAG, "observe" + integer);
            }
        });
        // 方法二：通過 LiveData.observeForever() 觀察 ViewModel 中的數據變化
//        liveData.observeForever(new Observer<Integer>() {
//            @Override
//            public void onChanged(@Nullable Integer integer) {
//                tvTime_LiveData.setText("Time :" + integer);
//                // Lifecycle.State.ON_STARTED 或 Lifecycle.State.ON_RESUME 和其他狀態，頁面都能收到來自 LiveDate 的通知
//                Log.d(TAG, "observeForever" + integer);
////                liveData.removeObserver(this);
//            }
//        });
//        // 前一頁已啟動過了
//        timerViewModel_LiveData.startTiming();
        // ---------------------

    }

    private void initRoomCallBack() {

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        roomFragmentAdapter = new roomFragmentAdapter(view.getContext(), getActivity(), this, false);
        recyclerView.setAdapter(roomFragmentAdapter);

        // init -----------------------------------------
        QueryStudentTask_ queryStudentTask_ = new QueryStudentTask_(getContext());
        queryStudentTask_.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"init");
        queryStudentTask_.setResultListener(new QueryStudentTask_.ResultListener() {
            @Override
            public void onFinished(Boolean result, String status, List<Student> studentList) {
                if (result && status.equals("200")) {
                    roomFragmentAdapter.roomFragmentAdapter(studentList);
                }
            }
        });

        // add ---------------------------------------------
        view.findViewById(R.id.btnAddStudent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QueryStudentTask_ queryStudentTask_ = new QueryStudentTask_(getContext());
                queryStudentTask_.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"insert", "test", timeStep + "");
                queryStudentTask_.setResultListener(new QueryStudentTask_.ResultListener() {
                    @Override
                    public void onFinished(Boolean result, String status, List<Student> studentList) {
                        if (result && status.equals("200")) {
                            roomFragmentAdapter.roomFragmentAdapter(studentList);
                        }
                    }

                });
            }
        });

    }

    private void initRoomLiveData() {

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        roomFragmentAdapter = new roomFragmentAdapter(view.getContext(), getActivity(), this, true);
        recyclerView.setAdapter(roomFragmentAdapter);

        // 實例化，並監聽 LiveData 變化 --------------------
        StudentViewModel studentViewModel = new ViewModelProvider(this).get(StudentViewModel.class);
        studentViewModel.getLiveDataStudent().observe(getViewLifecycleOwner(), new Observer<List<Student>>() {
            @Override
            public void onChanged(List<Student> students) {
                roomFragmentAdapter.roomFragmentAdapter(students);
            }
        });

        // init -------------------------------------------
        QueryStudentTask_LiveData ueryStudentTask_LiveData = new QueryStudentTask_LiveData(getContext());
        ueryStudentTask_LiveData.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"init");

        // add ---------------------------------------------
        view.findViewById(R.id.btnAddStudent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QueryStudentTask_LiveData queryStudentTask_LiveData = new QueryStudentTask_LiveData(getContext());
                queryStudentTask_LiveData.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"insert", "test", timeStep + "");
            }
        });


    }



}
