package net.jinfm.app.mvvmexample.paging.pagekeyeddatasource;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.jinfm.app.mvvmexample.R;
import net.jinfm.app.mvvmexample.paging.pagekeyeddatasource.model.User;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PageKeyFragment extends Fragment {

    private String TAG = this.getClass().getName();
    private View view;
    private RecyclerView recycler_view;
    private UserPagedListAdapter adapter;
    private UserViewModel userViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pagekey, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        init();

    }

    private void init() {

        recycler_view = view.findViewById(R.id.recycler_view);
        recycler_view.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler_view.setHasFixedSize(true);

        adapter = new UserPagedListAdapter(getContext());
        userViewModel = new ViewModelProvider(getActivity()).get(UserViewModel.class);
        userViewModel.UserPagedList.observe(getActivity(), new Observer<PagedList<User>>() {
            @Override
            public void onChanged(PagedList<User> users) {
                adapter.submitList(users);
            }
        });
        recycler_view.setAdapter(adapter);

        // -----------------------------------------------------------

        view.findViewById(R.id.to_ItemKeyFragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Navigation.findNavController(v).navigate(R.id.action_PageKeyFragment_to_ItemKeyFragment);
                Navigation.findNavController(v).navigate(R.id.action_PageKeyFragment_to_ItemkeyDemoFragment);

            }
        });

    }
}
