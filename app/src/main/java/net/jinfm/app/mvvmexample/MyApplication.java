package net.jinfm.app.mvvmexample;

import android.app.Application;

import net.jinfm.app.mvvmexample.tool.ApplicationObserver;

import androidx.lifecycle.ProcessLifecycleOwner;

public class MyApplication extends Application {

    // Activity 可以有多個，但 Application 只會有一個
    // 所以即使有多個 Activity，但 ProcessLifecycleOwner 只會有一個

    private static MyApplication mInstance ;
    public static final int CACHE_TIME = 5*60*1000 ; //快取失效時間

    public static synchronized MyApplication getInstance () {
        return mInstance ;
    }
    @Override
    public void onCreate () {
        super.onCreate ();
        mInstance = this ;

        /**
         * 第二章：LifeCycle
         */
        ProcessLifecycleOwner.get().getLifecycle().addObserver(new ApplicationObserver());

    }
}
