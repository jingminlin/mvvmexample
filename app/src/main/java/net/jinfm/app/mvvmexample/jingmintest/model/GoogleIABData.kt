package net.jinfm.app.mvvmexample.jingmintest.model


import com.squareup.moshi.Json

data class GoogleIABData(
    @Json(name = "acknowledged")
    val acknowledged: Boolean,
    @Json(name = "autoRenewing")
    val autoRenewing: Boolean,
    @Json(name = "orderId")
    val orderId: String,
    @Json(name = "packageName")
    val packageName: String,
    @Json(name = "productId")
    val productId: String,
    @Json(name = "purchaseState")
    val purchaseState: Int,
    @Json(name = "purchaseTime")
    val purchaseTime: Long,
    @Json(name = "purchaseToken")
    val purchaseToken: String
)