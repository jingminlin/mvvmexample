package net.jinfm.app.mvvmexample.paging.positionaldatasource;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.jinfm.app.mvvmexample.R;
import net.jinfm.app.mvvmexample.paging.positionaldatasource.model.Chapter;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 第九章：Paging
 * 需要繼承 PagedListAdapter
 */
public class ChapterPagedListAdapter extends PagedListAdapter<Chapter, ChapterPagedListAdapter.ChapterViewHolder> {

    private Context context;

    public ChapterPagedListAdapter(Context context){
        super(DIFF_CALLBACK);
        this.context = context;
    }

    /**
     * DiffUtil 工具介紹
     * 計算兩個數據列之間的差異
     * 以前的做法是 notifyDataSetChanged() 對整個數據源進行刷新，效率不高，且增刪的動畫效果較困難
     * DoffUtil 只會更新需要更新的數據，不需要刷新整個數據源，且可輕鬆地加入動畫效果
     *
     */
    private static final DiffUtil.ItemCallback<Chapter> DIFF_CALLBACK = new DiffUtil.ItemCallback<Chapter>() {
        /**
         * areItemsTheSame
         * 當檢測兩個對象是否代表同一個 Item 時，調用該方法進行判斷
         * @param oldItem
         * @param newItem
         * @return
         */
        @Override
        public boolean areItemsTheSame(@NonNull Chapter oldItem, @NonNull Chapter newItem) {
            return oldItem.chapter_id.equals(newItem.chapter_id);
        }

        /**
         * areContentsTheSame
         * 當檢測兩個 Item 是否存在不一樣的數據時，調用該方法進行判斷
         * @param oldItem
         * @param newItem
         * @return
         */
        @SuppressLint("DiffUtilEquals")
        @Override
        public boolean areContentsTheSame(@NonNull Chapter oldItem, @NonNull Chapter newItem) {
            return oldItem.equals(newItem);
        }
    };

    @NonNull
    @Override
    public ChapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.chapter_item, parent, false);
        return new ChapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChapterViewHolder holder, int position) {
        /**
         * 若有數據，直接將數據與 UI 綁定
         * 若沒有數據，則 getItem() 會通知 PageList 去獲取下一頁的數據，PagedList 收到通知後，讓 DataSource 執行具體的數據獲取工作
         */
        Chapter chapter = getItem(position);
        if (chapter != null){
            holder.position.setText(position + "");
            holder.tvId.setText(chapter.chapter_id);
            holder.tvTtitle.setText(chapter.title);
        } else {
            holder.position.setText(position + "");
            holder.tvId.setText("空的 chapter_id");
            holder.tvTtitle.setText("空的 title");
        }
    }

    class ChapterViewHolder extends RecyclerView.ViewHolder{
        TextView position;
        TextView tvId;
        TextView tvTtitle;
        public ChapterViewHolder(View itemView) {
            super(itemView);
            position = itemView.findViewById(R.id.position);
            tvId = itemView.findViewById(R.id.chapter_id);
            tvTtitle = itemView.findViewById(R.id.tvTtitle);
        }
    }
}
