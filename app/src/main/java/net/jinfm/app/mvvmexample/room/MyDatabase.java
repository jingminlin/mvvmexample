package net.jinfm.app.mvvmexample.room;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

/**
 * 第六章：Room
 * 宣告 MyDatabase 是 Room DB 物件
 * entities 指定 DB 有哪些 Table，用逗號隔開
 * version 指定版本號，用來判斷資料庫升級的版本
 */

/**
 * 第六章：Room 資料庫升級
 * 版本號從 1 改成 2
 */
@Database(entities = {Student.class}, version = 1)
//@Database(entities = {Student.class}, version = 2)
//@Database(entities = {Student.class}, version = 3)
/**
 * 第六章：Room 匯出 Schemas
 * 設定不會出 Schema
 */
//@Database(entities = {Student.class}, version = 2, exportSchema = false)
public abstract class MyDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "my_db";

    private static MyDatabase databaseInstance;

    /**
     * 單例、同步、鎖
     */
    public static synchronized MyDatabase getInstance(Context context) {
        if (databaseInstance == null) {

            databaseInstance = Room.databaseBuilder(
                    context.getApplicationContext(),
                    MyDatabase.class,
                    DATABASE_NAME)
                    /**
                     * 第六章：Room 資料庫升級
                     * 在 databaseBuilder 中加入這個 Migration
                     * .addMigrations(MIGRATION_1_2, MIGRATION_2_3) 若有其他版本，則用豆號隔開
                     * 可建立直接從 1 升級到 3 的功能 MIGRATION_1_3
                     */
                    .addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_1_3)
                    /**
                     * 第六章：Room 資料庫升級
                     * 若有錯誤 Room 會拋出 IllegalStateException
                     * Room 會直接把 table 重建，原本的資料會全部清空，除非還在內部開發中，否則一般都不建議這樣做
                     */
                    .fallbackToDestructiveMigration()
//                    .fallbackToDestructiveMigrationOnDowngrade()
//                    .fallbackToDestructiveMigrationFrom(3)
                    .allowMainThreadQueries()
                    /**
                     * 第六章：Room 預填充數據庫
                     */
                    .createFromAsset("databases/students.db")
                    // W/ROOM: Unable to copy database file.
                    // /storage/emulated/0/Download/students.db (Permission denied)
//                    .createFromFile(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "/Download/" + "students.db"))
                    .build();
        }
        return databaseInstance;
    }

    /**
     * 以抽象方法返回 Dao 物件
     */
    public abstract StudentDao studentDao();

    /** --------------------------------------------------------------------
     * 第六章：Room 資料庫升級
     * 建立版本 1 升級到 2 的內容
     */
    private static final Migration MIGRATION_1_2 = new Migration(1, 2){
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            // 執行升級相關的操作
            // 0. 新增欄位
            // 如果欄位在 Student Table 已經存在，就不能新增這個欄位
            // android.database.sqlite.SQLiteException: duplicate column name: addField_double
            database.execSQL("ALTER TABLE student "
                    + "ADD COLUMN addField_double TEXT");
        }
    };
    /** --------------------------------------------------------------------
     * 第六章：Room 銷毀與重建
     * 建立版本 2 升級到 3 的內容
     */
    private static final Migration MIGRATION_2_3 = new Migration(2, 3){
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            // 執行升級相關的操作
            // 1. 創建一張符合結構的臨時 Table_temp
            database.execSQL("CREATE TABLE temp_Student (" +
                    "id INTEGER PRIMARY KEY NOT NULL," +
                    "name TEXT," +
                    "age INTEGER NOT NULL,"+
                    "addField TEXT,"+
                    "addField_double TEXT)");
            // 2. 將數據從舊 Table_Student 複製到 Table_temp
            database.execSQL("INSERT INTO temp_Student(id, name, age, addField, addField_double) " +
                    "SELECT id, name, age, addField, addField_double FROM student");
            // 3. 刪除舊 Table_Student
            database.execSQL("DROP TABLE student");
            // 4. 將臨時 Table_temp 重新命名為 Table_Student
            database.execSQL("ALTER TABLE temp_Student RENAME TO student");
        }
    };

    private static final Migration MIGRATION_1_3 = new Migration(1, 3){
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            // 執行升級相關的操作
            // 0. 新增欄位
            // 如果欄位在 Student Table 已經存在，就不能新增這個欄位
            // android.database.sqlite.SQLiteException: duplicate column name: addField_double
            database.execSQL("ALTER TABLE student "
                    + "ADD COLUMN addField_double TEXT");

            // 1. 創建一張符合結構的臨時 Table_temp
            database.execSQL("CREATE TABLE temp_Student (" +
                    "id INTEGER PRIMARY KEY NOT NULL," +
                    "name TEXT," +
                    "age INTEGER NOT NULL,"+
                    "addField TEXT,"+
                    "addField_double TEXT)");
            // 2. 將數據從舊 Table_Student 複製到 Table_temp
            database.execSQL("INSERT INTO temp_Student(id, name, age, addField, addField_double) " +
                    "SELECT id, name, age, addField, addField_double FROM student");
            // 3. 刪除舊 Table_Student
            database.execSQL("DROP TABLE student");
            // 4. 將臨時 Table_temp 重新命名為 Table_Student
            database.execSQL("ALTER TABLE temp_Student RENAME TO student");
        }
    };



}
