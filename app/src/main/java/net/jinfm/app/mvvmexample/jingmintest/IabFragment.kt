package net.jinfm.app.mvvmexample.jingmintest

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.SharedPreferences
import android.graphics.Rect
import android.os.AsyncTask
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kotlinx.coroutines.*
import net.jinfm.app.mvvmexample.R
import net.jinfm.app.mvvmexample.databinding.FragmentIabBinding
import net.jinfm.app.mvvmexample.jingmintest.model.ServerIABData
import net.jinfm.app.mvvmexample.jingmintest.model.ServerIABDataViewModel
import net.jinfm.app.mvvmexample.jingmintest.tool.UnitConversion
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream
import java.util.concurrent.TimeUnit


class IabFragment : Fragment() {

    lateinit var dataBinding: FragmentIabBinding

//    var newsPaperData: NewsPaperData? = null

    private var paperMargin: Int = 0

    companion object {
//        fun newInstance(): IabFragment? {
//            val IabFragment = IabFragment()
//            return IabFragment
//        }

        private const val TAG = "IabFragment"
        private var activity: Activity? = null
        private var serverIABDataViewModel: ServerIABDataViewModel? = null

        //        private var billingClientLifecycle: BillingClientLifecycle? = null
        private var settings: SharedPreferences? = null
        private var currentAccount: String? = null
        private var webCurrentAccount: String? = null


        @JvmStatic
//        fun newInstance(application: Application, activity: Activity): IabFragment {
        fun newInstance(activity: Activity): IabFragment {
            val IabFragment = IabFragment()
            this.activity = activity
            return IabFragment
        }

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        dataBinding = FragmentIabBinding.inflate(inflater, container, false)
        dataBinding.lifecycleOwner = this

        // 不使用訂閱制 UI -------------------------------------------------
//        initData()
//
//        initBillingClient()
        // ---------------------------------------------------------------

//        testCoroutines()

        return dataBinding.root

    }

    private fun initData() {
//        newsPaperData = arguments?.getParcelable("newsPaperData")
//
//        newsPaperData?.entity?.forEach { entity ->
//            for (position in entity.position) {
//                positionList.add(position)
//            }
//        }

        // 建議的 retrofit 方法 ------------------------------------------------------
//        val serverIABDataViewModel = ViewModelProvider(this).get(ServerIABDataViewModel(true)::class.java)
//        dataBinding.serverIABDataViewModel = serverIABDataViewModel
//        serverIABDataViewModel.ServerIabData.observe(viewLifecycleOwner, Observer {
//            Log.d(TAG, "serverIABData: $it")
//            if (it != null) {
//                it.products.forEach { data ->
//                    Log.d(TAG, "productName: ${data.productName}")
//                    Log.d(TAG, "product_onlinetime: ${data.productOnlinetime}")
//                }
//                roundedCornersCustomized()
//                init_recyclerViewDataBinding()
//            }
//        })

//        val serverIABDataViewModel = ViewModelProvider((activity as ViewModelStoreOwner?)!!).get(ServerIABDataViewModel(false)::class.java)
        serverIABDataViewModel = ViewModelProvider((activity as ViewModelStoreOwner?)!!).get(ServerIABDataViewModel(false)::class.java)
        dataBinding.serverIABDataViewModel = serverIABDataViewModel

        Log.d(TAG, "jingmin: " + dataBinding.serverIABDataViewModel?.ServerIabData?.value?.salesInfo?.banner.toString())
//        if (dataBinding.serverIABDataViewModel?.ServerIabData?.value != null) {
//            roundedCornersCustomized()
//            init_recyclerViewDataBinding()
//        } else {
//            val serverIABDataViewModel = ViewModelProvider(this).get(ServerIABDataViewModel(true)::class.java)
//            dataBinding.serverIABDataViewModel = serverIABDataViewModel
//            serverIABDataViewModel.ServerIabData.observe(viewLifecycleOwner, Observer {
//                Log.d(TAG, "serverIABData: $it")
//                if (it != null) {
//                    it.products.forEach { data ->
//                        Log.d(TAG, "productName: ${data.productName}")
//                        Log.d(TAG, "product_onlinetime: ${data.productOnlinetime}")
//                    }
//                    roundedCornersCustomized()
//                    init_recyclerViewDataBinding()
//                }
//            })
//        }

        // 判斷是否登入中 -----------------------------------------------

//        settings = activity?.getSharedPreferences(activity?.getString(R.string.sp_account_data), Context.MODE_PRIVATE)
//        currentAccount =settings?.getString(getString(R.string.sp_currentAccount), null)
//        webCurrentAccount = settings?.getString(getString(R.string.sp_currentWebAccount), null)
//
//        if (currentAccount == null && webCurrentAccount != null) {
//            currentAccount = webCurrentAccount
//        }
//        Log.d(TAG, "IAB v3 currentAccount $currentAccount")

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // 不使用訂閱制 UI -------------------------------------------------
////        if (dataBinding.serverIABDataViewModel != null) {
//            initView()
//            initHandler()
////        } else {
////            Toast.makeText(context, "未取得資料", Toast.LENGTH_SHORT).show()
////        }

        // 不使用訂閱制 UI -------------------------------------------------


        dataBinding.urlHome.setOnClickListener {
            dataBinding.webViewTest.loadUrl("https://vip.udn.com")
        }

        dataBinding.urlDisscuss.setOnClickListener {

//            getEchartForceWebView
//            val SETUP_HTML = "file:///android_asset/EchartForce.html"
//            loadUrl(com.udn.ccstore.customclass.EchartForce.SETUP_HTML)

//            dataBinding.webViewTest.loadDataWithBaseURL(
//                    "https://vip.udn.com",
//                    "file:///android_asset/discuss.html",
//                    "text/html", "utf-8", null)
            try {
              dataBinding.webViewTest.loadDataWithBaseURL(
                      "https://vip.udn.com",
                      getHTML(),
                      "text/html", "utf-8", null)

//                val assetManager = context?.assets
////                val stream = assetManager?.open("file:///android_asset/discuss.html")
//                val stream = assetManager?.open("assets/discuss.html")
//                val r = BufferedReader(InputStreamReader(stream))
//                val total = StringBuilder()
//                var line: String?
//                while (r.readLine().also { line = it } != null) {
//                    total.append(line).append("\n")
//                }
//
////                val `is`: InputStream = getAssets().open("aaa.html")
////                val size = `is`.available()
////                val buffer = ByteArray(size)
////                `is`.read(buffer)
////                `is`.close()
////                val str = String(buffer)
////                println(str)
//
//                dataBinding.webViewTest.loadDataWithBaseURL(
//                        "https://vip.udn.com",
//                        total.toString(), "text/html", "UTF-8", null)

            } catch (xxx: Exception) {
                xxx.printStackTrace()
            }
        }


        dataBinding.webViewTest.settings.javaScriptEnabled = true

        // Add a WebViewClient
        dataBinding.webViewTest.setWebViewClient(object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {

                // Inject CSS when page is done loading
                injectCSS("animation.css")
                injectCSS("fontello.css")
                injectCSS("udn-discuss.css")
                injectJS("udn-discuss.bundle.js")

                super.onPageFinished(view, url)
            }
        })

    }



    private fun initView() {

        if (dataBinding.serverIABDataViewModel?.ServerIabData?.value != null) {
            init_subscriptionStatus()
            roundedCornersCustomized()
            init_recyclerViewDataBinding()
        } else {
            serverIABDataViewModel = ViewModelProvider(this).get(ServerIABDataViewModel(true)::class.java)
            dataBinding.serverIABDataViewModel = serverIABDataViewModel
            dataBinding.serverIABDataViewModel?.ServerIabData?.observe(viewLifecycleOwner, Observer {
                Log.d(TAG, "serverIABData: $it")
                if (it != null) {
                    it.products.forEach { data ->
                        Log.d(TAG, "productName: ${data.productName}")
                        Log.d(TAG, "product_onlinetime: ${data.productOnlinetime}")
                    }
                    init_subscriptionStatus()
                    roundedCornersCustomized()
                    init_recyclerViewDataBinding()
                }
            })
        }

    }

    private fun initHandler() {

//        dataBinding.setIabEventHandler(context?.let { activity?.let { it1 -> IabEventHandleListener(it, it1, dataBinding) } })
//        dataBinding.setIabEventHandler(IabEventHandleListener(context?, getActivity(context?), dataBinding))
//        dataBinding.setIabEventHandler(IabEventHandleListener(context, activity, dataBinding))
//        dataBinding.setIabEventHandler(IabEventHandleListener(context, getActivity(context), dataBinding))
        dataBinding.setIabEventHandler(IabEventHandleListener(context, dataBinding))

        dataBinding.btnSubscription.text = "訂閱"
        dataBinding.btnSubscription.setOnClickListener {

            if (currentAccount != null) {

            }

//            var flowParams : BillingFlowParams ? = BillingFlowParams.newBuilder()
//                    .setSkuDetails(billingClientLifecycle!!.getSkuDetails("com.udn.news.iabtest.sub.cs.1month.plan01"))
//                    .build()
//            val responseCode = billingClientLifecycle!!.launchBillingFlow(activity, flowParams)

            // 一般 OKHTTP 方法 ------------------------------------------------

            getUpdateConfig().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)

        }

//        // 返回鍵
//        dataBinding.iconBack.setOnClickListener {
//            Log.d(TAG, "iconBack")
//        }
//
//        // 問題鍵
//        dataBinding.iconQuestion.setOnClickListener {
//            Log.d(TAG, "iconQuestion")
//        }

    }

    fun getActivity(context: Context?): Activity? {
        if (context == null) {
            return null
        } else if (context is ContextWrapper) {
            return if (context is Activity) {
                context
            } else {
                getActivity(context.baseContext)
            }
        }
        return null
    }
    // -----------------------------------------------------------------

    private fun initBillingClient() {

//        billingClientLifecycle = BillingClientLifecycle.getInstance(activity?.application, activity)
////        purchases = billingClientLifecycle.purchases;
//        //        purchases = billingClientLifecycle.purchases;
////        skusWithSkuDetails = billingClientLifecycle.skusWithSkuDetails
//
//        lifecycle.addObserver(billingClientLifecycle!!)
//
//        // Register purchases when they change.
//
//        // Register purchases when they change.
//        billingClientLifecycle?.purchaseUpdateEvent?.observe(this, Observer { purchases ->
//            purchases?.let {
////            registerPurchases(it)
//
//                if (purchases != null) {
//                    try {
//
//                        // 發生購買事件，或有未 consume 的商品時
//                        // 會觸發 purchaseUpdateEvent，即可處理後續動作
//
////                        {"orderId":"GPA.3388-2960-1555-88227",
//                        //                        "packageName":"net.jinfm.app.test",
//                        //                        "productId":"net.jinfm.app.test.iap.cs.jin1090",
//                        //                        "purchaseTime":1614397830783,"purchaseState":0,
//                        //                        "purchaseToken":"eeanbjnnkglcajkoicidmcoo.AO-J1OxeSg_gbt_YCeROyvN5HVMVp2d7kf2dqNtP-tfWNP7hWddnnwqtEio3sHrtwDJapDyp9L1LVGHaPgFgo0qvPEXkg23Q0A","acknowledged":false}
//
//                        for (purchase in purchases) {
//                            val sku = purchase.sku
//                            val purchaseToken = purchase.purchaseToken
//                            val purchaseOrderId = purchase.orderId
//                            Log.d(TAG, "IAB v3 purchase with sku: $sku, token: $purchaseToken")
//                            Log.d(TAG, "IAB v3 purchase with purchaseOrderId: $purchaseOrderId")
//
////                            // 比對商品列表
////                            var udnPointData_purchase: GetIAPUPointProductResult.UdnPoint? = null
////                            for (i in 0 until myGlobalValue.getUdnPointData().size()) {
////                                if (sku == udnPointData.get(i).getProductId()) {
////                                    myGlobalValue.getUdnPointData_ErrorData().add(myGlobalValue.getUdnPointData().get(i))
////                                    myGlobalValue.getPurchaseProduct_ErrorData().add(purchase)
////                                    position = i
////                                    udnPointData_purchase = udnPointData.get(i)
////                                    break
////                                }
////                            }
//
//                            // 處理比對到的未 consume 商品
////                            IAB_v3_API_IAPUPointShipping(
////                                    context,
////                                    udnPointData_purchase.getPointId().toString() + "",
////                                    purchase,
////                                    purchase.orderId,
////                                    purchase.purchaseToken)
//
//                            // 呼叫小陸的 API 成功 --------------------------------------------------------------------
//                            val contentTask = iabSubscriptionTest(context, purchase, currentAccount)
//                            contentTask.setOnIabSubscriptionTestListener(object : iabSubscriptionTest.setOnIabSubscriptionTestListener {
//                                override fun onStart() {
//                                    Log.d(TAG, "IAB v3 onStart")
//                                }
//
//                                override fun onReceiveSuccess(result: String?) {
//                                    Log.d(TAG, "IAB v3 onReceiveSuccess： $result")
//                                }
//
//                                override fun onReceiveFailed(errorCode: Int) {
//                                    Log.d(TAG, "IAB v3 onReceiveFailed")
//                                }
//                            })
//                            contentTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
//
//                        }
//
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }
//                }
//
//            }
//        })


    }

    // subscription status
    private fun init_subscriptionStatus() {
        dataBinding.subscriptionStatusLayoutText.visibility = View.VISIBLE
        dataBinding.subscriptionStatusLayout.visibility = View.VISIBLE
    }

    // banner
    private fun roundedCornersCustomized() {

        // ImageView 圓角
        // https://blog.csdn.net/Jason_Lee155/article/details/107100717
        // https://stackoverflow.com/questions/34022552/how-to-fit-image-into-imageview-using-glide
        val options = RequestOptions()
        options.centerCrop()

//        var url: String = "https://s1.1zoom.me/big3/239/337032-SoLoVuShKa.jpg"
        var url: String = dataBinding.serverIABDataViewModel?.ServerIabData?.value?.salesInfo?.banner.toString()

        context?.let {
            if (url.endsWith(".gif")) {
                Glide.with(it)
                        .asGif()
//                        .placeholder(R.mipmap.ic_launcher)
//                        .error(R.mipmap.ic_launcher)
//                        .apply(RequestOptions.bitmapTransform(RoundedCorners(4)))
//                        .transform(CenterInside(), RoundedCorners(UnitConversion.convertDpToPixel(4F, it)))
//                        .transform(CenterInside(), RoundedCorners(4))
                        .transform(CenterInside(), RoundedCorners(10))
                        .load(url)
//                        .centerCrop()
//                        .fitCenter()
                        .into(dataBinding.imgTopbigimage)
            } else {
                Glide.with(it)
                        .load(url)
//                        .placeholder(R.mipmap.ic_launcher)
//                        .error(R.mipmap.ic_launcher)
//                        .apply(bitmapTransform(RoundedCorners(50)))
//                        .transform(CenterCrop(), RoundedCorners(50))
                        .transform(CenterInside(), RoundedCorners(10))
//                        .transform(CenterInside(), RoundedCorners(UnitConversion.convertDpToPixel(4F, it)))
//                        .centerCrop()
//                        .fitCenter()
                        .into(dataBinding.imgTopbigimage)
            }

            Glide.with(it)
                    .load(R.mipmap.deco_img_subscription)
                    .transform(CenterInside(), RoundedCorners(10))
                    .into(dataBinding.subscriptionStatusBackgroundimage)
        }


    }

    // RecyclerView
    private fun init_recyclerViewDataBinding() {

        val layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        dataBinding.iabRecyclerView.setLayoutManager(layoutManager)

        val adapter = dataBinding.serverIABDataViewModel?.ServerIabData?.value?.let {
            IabAdapter(it.products) { product: ServerIABData.Product, i: Int ->
                Log.d(TAG, "Clicked on item ${product}")
                Log.d(TAG, "Clicked on i ${i}")
            }
        }

        // 設定間距
        context?.let {
            val spacingInPixels = UnitConversion.convertDpToPixel(12, it)
            dataBinding.iabRecyclerView.addItemDecoration(SpacesItemDecoration(it, spacingInPixels))
        }

        dataBinding.iabRecyclerView.setAdapter(adapter)

    }

    private class SpacesItemDecoration(val context: Context, private val space: Int) : ItemDecoration() {
        override fun getItemOffsets(outRect: Rect, view: View,
                                    parent: RecyclerView, state: RecyclerView.State) {
            outRect.left = 0
            outRect.top = 0
            outRect.bottom = 0
            outRect.right = when {
                parent.getChildLayoutPosition(view) == parent.adapter?.itemCount -> 0
                else -> space
            }
        }
    }

    private fun testCoroutines() {

        // 多線程方法一：Thread
        Thread {
            for (i in 10 downTo 1) {
                Thread.sleep(1000)
                activity?.runOnUiThread {
                    Log.d(TAG, "Thread i $i")
                }
            }
            activity?.runOnUiThread {
                Log.d(TAG, "Done!")
            }
        }.start()

        // 多線程方法二：Coroutines
        GlobalScope.launch(Dispatchers.Main) {
            for (i in 10 downTo 1) {
                delay(1000)
                Log.d(TAG, "Coroutines i $i")
            }
        }

        // 多線程方法三：Coroutines + 停住
        val job: Job = GlobalScope.launch(Dispatchers.Main) {
            // launch coroutine in the main thread
            for (i in 10 downTo 1) { // countdown from 10 to 1
                Log.d(TAG, "Coroutines i $i");
                delay(1000) // wait half a second
            }
            Log.d(TAG, "Done!")
        }
        job.cancel() // 停住


    }

    private fun goToPaperContentFragment() {
//        val newsPaperItem = newsPaperData?.entity?.find { it.art_id == artId }
//        if (artId != -1 && newsPaperItem != null) {
//            Log.d(TAG, "newsPaperItem: " + newsPaperItem.art_title)
//
//            val paperViewPagerFragment = (activity as AppCompatActivity).supportFragmentManager.findFragmentByTag(PaperViewPagerFragment::class.java.simpleName)
//
//            val fragment = PaperContentFragment.newInstance(newsPaperItem)
//            fragment?.let {
//                if (paperViewPagerFragment != null) {
//                    (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
//                            //                    .replace(R.id.container_paper, it, PaperContentFragment::class.java.simpleName)
//                            .hide(paperViewPagerFragment)
//                            .add(R.id.container_paper, it, PaperContentFragment::class.java.simpleName)
//                            .addToBackStack(PaperContentFragment::class.java.simpleName)
//                            .commitAllowingStateLoss()
//                } else {
//                    (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
//                            .replace(R.id.container_paper, it, PaperContentFragment::class.java.simpleName)
//                            .addToBackStack(PaperContentFragment::class.java.simpleName)
//                            .commitAllowingStateLoss()
//                }
//            }
//        }
    }

    // 不使用訂閱制 UI -------------------------------------------------

    // Inject CSS method: read style.css from assets folder
    // Append stylesheet to document head
    private fun injectCSS(css_name: String) {
        try {
            val inputStream: InputStream? = context?.getAssets()?.open("disscuss/" + css_name)
            val buffer = inputStream?.available()?.let { ByteArray(it) }
            inputStream?.read(buffer)
            inputStream?.close()
            val encoded: String = Base64.encodeToString(buffer, Base64.NO_WRAP)
            dataBinding.webViewTest.loadUrl(
                    "javascript:(function() {" +
                            "var parent = document.getElementsByTagName('head').item(0);" +
                            "var style = document.createElement('style');" +
                            "style.type = 'text/css';" +  // Tell the browser to BASE64-decode the string into your script !!!
                            "style.innerHTML = window.atob('" + encoded + "');" +
                            "parent.appendChild(style)" +
                            "})()"
            )
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun injectJS(js_name: String) {
        try {
            val inputStream: InputStream? = context?.getAssets()?.open("disscuss/" + js_name)
            val buffer = inputStream?.available()?.let { ByteArray(it) }
            inputStream?.read(buffer)
            inputStream?.close()
            val encoded = Base64.encodeToString(buffer, Base64.NO_WRAP)
            dataBinding.webViewTest.loadUrl("javascript:(function() {" +
                    "var parent = document.getElementsByTagName('head').item(0);" +
                    "var script = document.createElement('script');" +
                    "script.type = 'text/javascript';" +
                    "script.innerHTML = window.atob('" + encoded + "');" +
                    "parent.appendChild(script)" +
                    "})()")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }
    fun getHTML(): String {
        var data: String = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "  <head>\n" +
                "    <meta charset=\"UTF-8\" />\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n" +
                "    <title>Document</title>\n" +
                "\n" +
                "    <!--APP_CSS_fontello-->\n" +
                "    <!--APP_CSS_animation-->\n" +
                "\n" +
                "    <meta name=\"channel_id\" content=\"8799\" />\n" +
                "    <meta name=\"art_id\" content=\"5480715\" />\n" +
                "    <meta name=\"api_domain\" content=\"//interactive.udn.com\" />\n" +
                "    <style>\n" +
                "      * {\n" +
                "        box-sizing: border-box;\n" +
                "      }\n" +
                "      html,\n" +
                "      body {\n" +
                "        padding: 0;\n" +
                "        margin: 0;\n" +
                "      }\n" +
                "\n" +
                "      .container {\n" +
                "        padding: 40px;\n" +
                "        max-width: 680px;\n" +
                "        width: 100%;\n" +
                "        margin: 0 auto;\n" +
                "   \n" +
                "        background: var(--bg);\n" +
                "      }\n" +
                "    </style>\n" +
                "    \n" +
                "  </head>\n" +
                "\n" +
                "  <body>\n" +
                "\n" +
                "    <!--APP_CSS_udn-discuss-->\n" +
                "\n" +
                "    <section id=\"discussApp\" class=\"discuss-board\">\n" +
                "      <div class=\"container\">\n" +
                "        \n" +
                "        <div id=\"comments_body\" :class=\"{'rules-open': rulesOpen}\" :data-sort=\"sortType\"  v-cloak>\n" +
                "\n" +
                "          <header class=\"context-box__header\">\n" +
                "            <h3 class=\"context-box__title\">評論</h3>\n" +
                "          </header>\n" +
                "        \n" +
                "          <div class=\"input-field\">\n" +
                "            <textarea :disabled=\"postState !== 'idle'\" v-model=\"postText\" aria-label=\"comment-textarea\" cols=\"45\" rows=\"5\" maxlength=\"500\"\n" +
                "              class=\"discuss-board__textarea\" placeholder=\"500字以內\"></textarea>\n" +
                "      \n" +
                "            <div class=\"discuss-board__tools\">\n" +
                "              <span class=\"btn btn-lg btn--selection rules-toggler\" @click=\"rulesToggler\" :class=\"{'btn--selection-open': rulesOpen}\">規範 <i class=\"i-arrow6-down\"></i></span>\n" +
                "              <button class=\"btn btn-ripple btn-comment--post btn-red\" @click=\"postDiscuss\" aria-label=\"post\" :data-state=\"postState\">發布</button>\n" +
                "            </div>\n" +
                "        \n" +
                "      \n" +
                "            <div class=\"discuss-board__rules\">\n" +
                "              <ul style=\"font-size:larger\">\n" +
                "                <li>張貼文章或下標籤，不得有違法或侵害他人權益之言論，違者應自負法律責任。</li>\n" +
                "                <li>對於明知不實或過度情緒謾罵之言論，經網友檢舉或本網站發現，聯合報有權逕予刪除文章、停權或解除會員資格。不同意上述規範者，請勿張貼文章。</li>\n" +
                "                <li>對於無意義、與本文無關、明知不實、謾罵之標籤，聯合報有權逕予刪除標籤、停權或解除會員資格。不同意上述規範者，請勿下標籤。</li>\n" +
                "                <li>凡「暱稱」涉及謾罵、髒話穢言、侵害他人權利，聯合報有權逕予刪除發言文章、停權或解除會員資格。不同意上述規範者，請勿張貼文章。</li>\n" +
                "              </ul>\n" +
                "            </div>\n" +
                "          </div>\n" +
                "      \n" +
                "          <header class=\"discuss-board__header\">\n" +
                "            <div class=\"discuss-board__total\">全部評論 (<span class=\"rr-no\">{{total}}</span>)</div>\n" +
                "            <button class=\"btn btn-lg btn--selection\" :class=\"{'btn--selection-open': sortBox}\" @click=\"toggleSort\">排序方式 <i class=\"i-arrow6-down\"></i></button>\n" +
                "            <div class=\"sort-box\" :class=\"{'sort-box--open': sortBox}\">\n" +
                "              <span class=\"sort-box__btn btn list-ellipsis\" :class=\"{'active': sortType === 'popular'}\" @click=\"sortDiscuss('popular')\">按讚最多</span>\n" +
                "              <span class=\"sort-box__btn btn list-ellipsis\" :class=\"{'active': sortType === 'comment'}\" @click=\"sortDiscuss('comment')\">最多回覆</span>\n" +
                "              <span class=\"sort-box__btn btn list-ellipsis\" :class=\"{'active': sortType === 'newest'}\" @click=\"sortDiscuss('newest')\">新到舊　</span>\n" +
                "              <span class=\"sort-box__btn btn list-ellipsis\" :class=\"{'active': sortType === 'oldest'}\" @click=\"sortDiscuss('oldest')\">舊到新　</span>\n" +
                "  \n" +
                "  \n" +
                "            </div>\n" +
                "          </header>\n" +
                "      \n" +
                "          <div class=\"discuss-board__content\" :class=\"{'discuss-board__content--loading': isLoading}\">\n" +
                "            \n" +
                "            <!-- by list -->\n" +
                "       \n" +
                "            <section class=\"discuss-board__list\" v-for=\"(discuss, index) in lists\" :key=\"discuss.id\">\n" +
                "      \n" +
                "              <rr-discuss :discuss=\"discuss\" @reply=\"receiveReply(\$event)\" can_reply=\"1\" @delete-post=\"filterReply\"></rr-discuss>\n" +
                "       \n" +
                "              <div class=\"discuss-details__reply\">\n" +
                "                <rr-discuss :discuss=\"reply\" can_reply=\"0\" v-for=\"reply in discuss.replies\" :key=\"reply.id\" @delete-post=\"filterReply\"></rr-discuss>\n" +
                "                <span class=\"btn btn--reply\" v-if=\"discuss.total_reply > discuss.replies.length && !discuss.is_reply_end\" @click=\"loadReply(discuss)\" :data-state=\"discuss.loadReply\"><i class=\"i-follow\"></i>看更多回覆</span>\n" +
                "              </div>\n" +
                "            </section>\n" +
                "      \n" +
                "      \n" +
                "      \n" +
                "          </div>\n" +
                "      \n" +
                "      \n" +
                "          <div class=\"btn-holder btn-holder--center\" v-if=\"!isEnd\">\n" +
                "            <button class=\"btn btn-ripple btn-more\" @click=\"getComment\" :data-state=\"(isLoading) ? 'loading': 'idle'\" aria-label=\"more-comment\"\n" +
                "              data-id=\"more-comment\">More <i class=\"i-arrow5-down\"></i></button>\n" +
                "          </div>\n" +
                "      \n" +
                "        </div>\n" +
                "      </div>\n" +
                "    </section>\n" +
                "\n" +
                "      <!--APP_JS_udn-discuss.bundle-->\n" +
                "\n" +
                "    <script src=\"udn-discuss.bundle.js\"></script>\n" +
                "\n" +
                "  </body>\n" +
                "</html>\n"

        return data
    }

    // 20210419 測試 API ---------------------------------------------------------------------------

    fun button_listner() {

        dataBinding.btnSubscription.setOnClickListener {

            // 一般 retrofit 方法 test ------------------------------------------------------
//            // baseUrl: 定義 API 網域網址，即是我們剛剛拆出來的前綴共用的部分
//            // ddConverterFactory: 定義要將資料轉成什麼格式
//            val retrofit = Retrofit.Builder()
//                    .baseUrl("https://jsonplaceholder.typicode.com")
//                    .addConverterFactory(MoshiConverterFactory.create())
//                    .build()
//
//            val apiService: ApiService = retrofit.create(ApiService::class.java)
//
//            //宣告 Call 連線服務
////            val call: Call<UserData> = apiService.getUser()
//            val call: Call<List<UserData>> = apiService.getUsers()
//
//            //執行連線服務，透過 Callback 來等待回傳成功或失敗的資料
//            call.enqueue(object : Callback<List<UserData>?> {
//                override fun onResponse(call: Call<List<UserData>?>?, response: Response<List<UserData>?>) {
//                    // 連線成功，透過 getter 取得特定欄位資料
//                    try {
//
////                        for (i in response.body()?.size?.downTo(0)!!) {
////                            Log.d(TAG, "id: " + response.body()?.get(i).id)
////                            Log.d(TAG, "name: " + response.body()?.get(i).name)
////
////                        }
//
////                        for (i in 0 until response.body()?.size!!){
////                            println(response.body()!!.get(i).id)
////                        }
//
//                        // 透過 foreach 取出每一筆資料內容
//                        for (item in response.body()!!) {
//                            Log.d(TAG, "id: " + item.id)
//                            Log.d(TAG, "name: " + item.name)
//                        }
//
////                        Log.d(TAG, "id: " + response.body().get(i).id)
////                        Log.d(TAG, "name: " + response.body().get(i).name)
//
////                        Log.d(TAG, "id: " + response.body()?.id)
////                        Log.d(TAG, "name: " + response.body()?.name)
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }
//                }
//
//                override fun onFailure(call: Call<List<UserData>?>?, t: Throwable) {
//                    // 連線失敗，印出錯誤訊息
//                    Log.d(TAG, "response: $t")
//                }
//
//            })

            // 一般 retrofit 方法 呼叫 經濟日報數位板 API-----------------------------

//            // baseUrl: 定義 API 網域網址，即是我們剛剛拆出來的前綴共用的部分
//            // ddConverterFactory: 定義要將資料轉成什麼格式
//            val retrofit = Retrofit.Builder()
//                    .baseUrl("https://lab7-misc.udn.com/AppFeedMaker/")
//                    .addConverterFactory(MoshiConverterFactory.create().asLenient())
//                    .build()
//
//            val iabPageApiService: IabPageApiService = retrofit.create(IabPageApiService::class.java)
//
//            //宣告 Call 連線服務
////            val call: Call<List<ServerIABData>> = iabPageApiService.getServerIabData_test()
////            //執行連線服務，透過 Callback 來等待回傳成功或失敗的資料
////            call.enqueue(object : Callback<List<ServerIABData>?> {
////                override fun onResponse(call: Call<List<ServerIABData>?>?, response: Response<List<ServerIABData>?>) {
////                    try {
////                        for (item in response.body()!!) {
////                            Log.d(TAG, "storeId: " + item.storeId)
////                        }
////                    } catch (e: Exception) {
////                        e.printStackTrace()
////                    }
////                }
////                override fun onFailure(call: Call<List<ServerIABData>?>?, t: Throwable) {
////                    Log.d(TAG, "response: $t")
////                }
////            })
//
//            val call: Call<ResponseBody?>? = iabPageApiService.getServerIabData_test("vipProducts")
//
//            call?.enqueue(object : Callback<ResponseBody?> {
//                override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
//                    try {
//                        Log.d(TAG, "response.headers(): " + response.headers())
//                        Log.d(TAG, "response.code(): " + response.code())
//                        Log.d(TAG, "response.body(): " + response.body())
//                        Log.d(TAG, "response.message(): " + response.message())
//                        Log.d(TAG, "response.raw(): " + response.raw())
//                        Log.d(TAG, "response.raw().body: " + response.raw().body)
//                        Log.d(TAG, "response: " + response)
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }
//                }
//
//                override fun onFailure(call: Call<Response?>, t: Throwable) {
//                    Log.d(TAG, "response: $t")
//                }
//
//            })

            // 建議的 retrofit 方法 ------------------------------------------------------

//            val serverIABDataViewModel = ViewModelProvider(this).get(ServerIABDataViewModel::class.java)
//            serverIABDataViewModel.ServerIabData.observe(viewLifecycleOwner, Observer {
//                Log.d(TAG, "serverIABData: $it")
//            })

//            val paperViewModel = ViewModelProvider(this).get(PaperViewModel::class.java)
//            paperViewModel.newsPapers.observe(viewLifecycleOwner, Observer {
//                Log.d(TAG, "newsPapers: $it")
//            })

            // 一般 OKHTTP 方法 ------------------------------------------------------

            getUpdateConfig().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)


        }
    }

    class getUpdateConfig internal constructor() : AsyncTask<Void?, Void?, JSONObject?>() {

        //        private val configUrl: String? = "https://lab7-misc.udn.com/AppFeedMaker/vipProducts"
        private val configUrl: String? = "https://api.github.com/users"


        init {

        }

        override fun doInBackground(vararg params: Void?): JSONObject? {
            if (configUrl != null) {
                val client: OkHttpClient = OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build()
                val request: Request = Request.Builder().url(configUrl).build()

                try {
                    val response = client.newCall(request).execute()
                    val responseBody = response.body
                    if (responseBody != null) {
                        val result = responseBody.string()
                        Log.d(TAG, "result: $result")
                        val resultObject = JSONObject(result)
                        return resultObject
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
            return null
        }

        override fun onPostExecute(result: JSONObject?) {
            super.onPostExecute(result)
            if (result != null) {
            }
        }

    }


}
