package net.jinfm.app.mvvmexample;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import net.jinfm.app.mvvmexample.coroutine.CoroutineActivity;
import net.jinfm.app.mvvmexample.databinding.ActivityMainBinding;
import net.jinfm.app.mvvmexample.jingmintest.model.ServerIABData;
import net.jinfm.app.mvvmexample.jingmintest.model.ServerIABDataViewModel;
import net.jinfm.app.mvvmexample.tool.MyLocationListener;
import net.jinfm.app.mvvmexample.tool.MyViewModel;
import net.jinfm.app.mvvmexample.tool.MyViewModelFactory;
import net.jinfm.app.mvvmexample.workmanager.UploadLogWorker_1;
import net.jinfm.app.mvvmexample.workmanager.UploadLogWorker_2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.work.BackoffPolicy;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

public class MainActivity extends AppCompatActivity {

    private String TAG = this.getClass().getName();
    private MyLocationListener myLocationListener;

    // 第三章：Navigation App Bar
    private AppBarConfiguration appBarConfiguration; // 用於 App Bar 的配置
    private NavController navController;  // 用於頁面導航和切換

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);

        /**
         * 第八章：DataBinding
         * Activity 綁定 XML 的方式
         */
        ActivityMainBinding activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        initAppBar();

        // 第二章：LifeCycle
        initObserver();

//        changePage();

        initWorkManager();

        testFunction();

    }

    // 第三章：Navigation App Bar ----------------------------

    // 在 MainActivity 實例化菜單
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        return super.onCreateOptionsMenu(menu);
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    // 設定 NavigationUI 組件，能自動處理好跳轉頁面邏輯
    private void initAppBar() {

        // 用於頁面導航和切換
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        // 用於 App Bar 的配置
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        // 將兩者綁定
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

        // 監聽換頁事件
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                Log.d(TAG, "前往：" + destination);
            }
        });
    }

    // 自動完成頁面跳轉
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        return super.onOptionsItemSelected(item);
        return NavigationUI.onNavDestinationSelected(item, navController) || super.onOptionsItemSelected(item);
    }

    // 自動完成返回上一頁動作
    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, appBarConfiguration) || super.onSupportNavigateUp();
    }


    // // 第二章：LifeCycle ---------------------------
    private void initObserver() {

        myLocationListener = new MyLocationListener(this, "MainActivity", new MyLocationListener.OnLocationChangedListener() {
            @Override
            public void onChanged(int data) {

                Log.d(TAG, "第二章：onChanged 展示收到的位置訊息 " + data);

            }
        });

        // 將觀察者與被觀察者綁定
        this.getLifecycle().addObserver(myLocationListener);

    }

    // ----------------------------------------

    private void changePage() {

        Fragment fragment = MainFragment.newInstance(this);
        FragmentTransaction ft = MainActivity.this.getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_layout, fragment);
        ft.commit();

    }

    // 第七章：WorkManager
    private void initWorkManager() {

        // 一次性任務 OneTimeWorkRequest -------------------
        // 1. 設置任務觸發條件
        Constraints constraints = new Constraints.Builder()
                .setRequiresCharging(true) // 充電中
                .setRequiredNetworkType(NetworkType.CONNECTED) // 連接網路
                .setRequiresBatteryNotLow(true) // 電量充足
                .build();

        // 1-1. WorkManager 與 Worker 之間的參數傳遞
        // 只能傳遞基本類型的數據，且不能超過 10KB
        Data inputData_1 = new Data.Builder()
                .putString("input_data", "OneTime_1")
                .build();

        // 2. 將任務觸發條件設置到 WorkRequest
        OneTimeWorkRequest uploadWorkRequest_OneTime_1 = new OneTimeWorkRequest.Builder(UploadLogWorker_1.class)
                .setConstraints(constraints) // 設置觸發條件
                .setInitialDelay(6, TimeUnit.SECONDS) // 設置延遲執行任務
                .addTag("uploadWorkRequest_OneTime_1") // 為任務設置 tag 標籤，可用來跟蹤任務狀態或取消任務
                .setInputData(inputData_1) // WorkManager 與 Worker 之間的參數傳遞
                .build();

        // 3. 將任務提交給系統
        WorkManager.getInstance(this).enqueue(uploadWorkRequest_OneTime_1);

        // 4. 獲得任務狀態
        WorkManager.getInstance(this)
                .getWorkInfoByIdLiveData(uploadWorkRequest_OneTime_1.getId())
                .observe(MainActivity.this, new Observer<WorkInfo>() {
                    @Override
                    public void onChanged(WorkInfo workInfo) {
                        // ENQUEUED - RUNNING - SUCCEEDED
                        if (workInfo != null && workInfo.getState() == WorkInfo.State.ENQUEUED) {
//                            Log.d(TAG, "第七章：WorkManager 一次性任務 workInfo status " + workInfo.getState());
                            Log.d(TAG, "第七章：WorkManager OneTime_1 " + workInfo.getState());
                            // 取消任務
//                            WorkManager.getInstance(MainActivity.this).cancelAllWorkByTag("UploadTag_OneTime");
                        } else if (workInfo != null && workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                            String outputData = workInfo.getOutputData().getString("output_data");
//                            Log.d(TAG, "第七章：WorkManager 一次性任務 workInfo data：" + outputData);
                            Log.d(TAG, "第七章：WorkManager OneTime_1：" + outputData);
                            // 取消任務
                            WorkManager.getInstance(MainActivity.this).cancelAllWorkByTag("UploadTag_OneTime");
                        }
                    }
                });


        // 週期性任務 PeriodicWorkRequest -------------------

        PeriodicWorkRequest uploadWorkRequest_Periodic = new PeriodicWorkRequest.Builder(UploadLogWorker_1.class, 15, TimeUnit.MINUTES)
                .setConstraints(constraints) // 設置觸發條件
                .setInitialDelay(2, TimeUnit.SECONDS) // 設置延遲執行任務
//                .setBackoffCriteria(BackoffPolicy.LINEAR, 30, TimeUnit.SECONDS) // 失敗的時候重新嘗試的相關設定
                .setBackoffCriteria(BackoffPolicy.LINEAR,
                        OneTimeWorkRequest.MIN_BACKOFF_MILLIS,
                        TimeUnit.MILLISECONDS) // 失敗的時候重新嘗試的相關設定
                .addTag("uploadWorkRequest_Periodic") // 為任務設置 tag 標籤，可用來跟蹤任務狀態或取消任務
//                .setInputData(inputData) // WorkManager 與 Worker 之間的參數傳遞
                .build();

        // 週期性任務 3. 將任務提交給系統
//        WorkManager.getInstance(this).enqueue(uploadWorkRequest_Periodic);

        // 週期性任務 4. 獲得任務狀態
        WorkManager.getInstance(this)
                .getWorkInfoByIdLiveData(uploadWorkRequest_Periodic.getId())
                .observe(MainActivity.this, new Observer<WorkInfo>() {
                    @Override
                    public void onChanged(WorkInfo workInfo) {
                        // ENQUEUED - RUNNING - ENQUEUED
                        Log.d(TAG, "第七章：WorkManager 週期性任務 workInfo 有回傳到 observe / workInfo.getState()=" + workInfo.getState());
                        if (workInfo != null && workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                            String outputData = workInfo.getOutputData().getString("output_data");
                            Log.d(TAG, "第七章：WorkManager 週期性任務 不會看到 SUCCEEDED");
                            // 取消任務
//                            WorkManager.getInstance(MainActivity.this).cancelAllWorkByTag("UploadTag_periodic");
                        } else if (workInfo != null && workInfo.getState() == WorkInfo.State.RUNNING) {
                            Log.d(TAG, "第七章：WorkManager 週期性任務 workInfo state RUNNING");
                            // 取消任務
//                            WorkManager.getInstance(MainActivity.this).cancelAllWorkByTag("UploadTag_periodic");
                        }
                    }
                });


        // 任務鏈，執行的先後順序  ---------------------------------------

        // 1-1. WorkManager 與 Worker 之間的參數傳遞
        // 只能傳遞基本類型的數據，且不能超過 10KB
        Data inputData_2 = new Data.Builder()
                .putString("input_data", "OneTime_2")
                .build();

        OneTimeWorkRequest uploadWorkRequest_OneTime_2 = new OneTimeWorkRequest.Builder(UploadLogWorker_2.class)
                .setConstraints(constraints) // 設置觸發條件
                .setInitialDelay(3, TimeUnit.SECONDS) // 設置延遲執行任務
                .addTag("uploadWorkRequest_OneTime_2") // 為任務設置 tag 標籤，可用來跟蹤任務狀態或取消任務
                .setInputData(inputData_2) // WorkManager 與 Worker 之間的參數傳遞
                .build();
        // 3. 將任務提交給系統
//        WorkManager.getInstance(this).enqueue(uploadWorkRequest_OneTime_2);

        // 4. 獲得任務狀態
        WorkManager.getInstance(this)
                .getWorkInfoByIdLiveData(uploadWorkRequest_OneTime_2.getId())
                .observe(MainActivity.this, new Observer<WorkInfo>() {
                    @Override
                    public void onChanged(WorkInfo workInfo) {
                        // ENQUEUED - RUNNING - SUCCEEDED
                        if (workInfo != null && workInfo.getState() == WorkInfo.State.ENQUEUED) {
                            Log.d(TAG, "第七章：WorkManager OneTime_2 " + workInfo.getState());
                            // 取消任務
//                            WorkManager.getInstance(MainActivity.this).cancelAllWorkByTag("UploadTag_OneTime");
                        } else if (workInfo != null && workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                            String outputData = workInfo.getOutputData().getString("output_data");
                            Log.d(TAG, "第七章：WorkManager OneTime_2：" + outputData);
                            // 取消任務
                            WorkManager.getInstance(MainActivity.this).cancelAllWorkByTag("UploadTag_OneTime");
                        }
                    }
                });

        List<OneTimeWorkRequest> otwrList = new ArrayList<>();
        otwrList.add(uploadWorkRequest_OneTime_1);
        otwrList.add(uploadWorkRequest_OneTime_2);

        // 任務鏈 3. 將任務提交給系統
//        WorkManager.getInstance(this)
//                .beginWith(otwrList) // 並行
////                .then(uploadWorkRequest_OneTime_1) // 上一個步驟都執行完之後才會執行，一次性任務在同一條線上無法連續執行
////                .then(uploadWorkRequest_Periodic) // ERROR，只能放入一次性任務
//                .enqueue();

        // 分組完成後，再進行連結 --------------------------------------
//        WorkContinuation chain1 = WorkManager.getInstance(this)
//                .beginWith(uploadWorkRequest_OneTime)
//                .then(uploadWorkRequest_OneTime_2);
//
//        WorkContinuation chain2 = WorkManager.getInstance()
//                .beginWith(uploadWorkRequest_OneTime)
//                .then(uploadWorkRequest_OneTime_2);
//
//        List<WorkContinuation> wcList = new ArrayList<>();
//        wcList.add(chain1);
//        wcList.add(chain2);
//        // 任務鏈 3. 將任務提交給系統
//        WorkContinuation chain3 = WorkContinuation
//                .combine(wcList);
////                .then(uploadWorkRequest_OneTime_2); // 一次性任務在同一條線上無法連續執行
//        chain3.enqueue();


    }

    // to Coroutines
    public void toCoroutines() {
        Intent intent = new Intent(this, CoroutineActivity.class);
        startActivityForResult(intent, 9999);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    // to
    public void toJingminTest(View v) {

        // 測試 API
//        ViewModel serverIABDataViewModel = new ViewModelProvider(this).get(ServerIABDataViewModel.class);
//        ViewModel serverIABDataViewModel = new ViewModelProvider((androidx.lifecycle.ViewModelStoreOwner) getApplication()).get(ServerIABDataViewModel.class);
//        TimerViewModel timerViewModel_LiveData = new ViewModelProvider((ViewModelStoreOwner) activity).get(TimerViewModel.class);
        ViewModel serverIABDataViewModel = new ViewModelProvider(this).get(ServerIABDataViewModel.class);
        final MutableLiveData<ServerIABData> liveData1 = (MutableLiveData<ServerIABData>)
                ((ServerIABDataViewModel) serverIABDataViewModel).getServerIabData();

        liveData1.observe(this, new Observer<ServerIABData>() {
            @Override
            public void onChanged(ServerIABData serverIABData) {
//                Log.d(TAG, "serverIABData productName: " + serverIABData.getProducts().get(0).getProductName());

//                Intent intent = new Intent(MainActivity.this, IabActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putString("param", "test");
////        bundle.putParcelable("ServerIABData", serverIABData);
////                bundle.putParcelable("ServerIABData", (Parcelable) liveData1);
////                bundle.putParcelable("ServerIABData", (LiveData<ServerIABData>) liveData1);
////                intent.putParcelableArrayListExtra("test", (liveData1 as LiveData<ServerIABData>));
////                intent.putParcelableArrayListExtra("test", (LiveData<ServerIABData>) liveData1);
//                intent.putExtras(bundle);
//                startActivityForResult(intent, 9999);
//                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

//        Fragment fragment = IabFragment.newInstance(MainActivity.this);
//        FragmentTransaction ft = MainActivity.this.getSupportFragmentManager().beginTransaction();
//        ft.replace(R.id.fragment_layout, fragment);
//        ft.commit();

                Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_jingminTestFragment);

            }
        });

//        Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_jingminTestFragment);


    }

    private void testFunction() {

        // -----------------------------------------------------------------------------------------

        Rect rectangle = new Rect();
        Window window = getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        int statusBarHeight = rectangle.top;
        int contentViewTop =
                window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int titleBarHeight = contentViewTop - statusBarHeight;

        Log.i("*** Elenasys :: ", "StatusBar Height= " + statusBarHeight + " , TitleBar Height = " + titleBarHeight);

        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        Log.i("*** Elenasys :: ", "StatusBar Height= " + result);

        Log.i("*** Elenasys :: ", "getStatusBarHeight= " + getStatusBarHeight(MainActivity.this));

        // -----------------------------------------------------------------------------------------

        // 在非 activity 類中，獲取資源字串文件資料
        String app_name = MyApplication.getInstance().getString(R.string.app_name);

        // 在非 activity 類中，獲取 Application 的變數
        int cache_time = MyApplication.CACHE_TIME;

        // 第五章：LiveData 傳遞參數給 ViewModelFactory ----------------------------------------------
        ViewModel myViewModelFactory =
                new ViewModelProvider(this, new MyViewModelFactory(getApplication(), 900))
                        .get(MyViewModel.class);

//        ViewModel myViewModelFactory =
//                new ViewModelProvider(this, (ViewModelProvider.Factory) new MyViewModel(getApplication(),900))
//                        .get(MyViewModel.class);

//        mBinding.vm = ViewModelProviders.of(this, MainVMFactory(888)).get(MainVM::class.java)
//        mBinding.vm = ViewModelProviders.of(this, VMFactory{ MainVM(888) }).get(MainVM::class.java)

        // ----------------------------------------


        final MutableLiveData<Integer> liveData = (MutableLiveData<Integer>)
                ((MyViewModel) myViewModelFactory).getCurrentSecond();
        // 方法一：通過 LiveData.observe() 觀察 ViewModel 中的數據變化
        liveData.observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                // 只有在 Lifecycle.State.ON_STARTED 或 Lifecycle.State.ON_RESUME 時，頁面才能收到來自 LiveDate 的通知
//                Log.d(TAG, "第五章：LiveData 傳遞參數給 ViewModelFactory observe " + integer);
            }
        });


        // 測試 API
//        ViewModel serverIABDataViewModel = new ViewModelProvider(this).get(ServerIABDataViewModel.class);
//        final MutableLiveData<ServerIABData> liveData1 = (MutableLiveData<ServerIABData>)
//                ((ServerIABDataViewModel) serverIABDataViewModel).getServerIabData();
//
//        liveData1.observe(this, new Observer<ServerIABData>() {
//            @Override
//            public void onChanged(ServerIABData serverIABData) {
//                Log.d(TAG, "serverIABData productName: " + serverIABData.getProducts().get(0).getProductName());
//            }
//        });


    }

    public static int getStatusBarHeight(final Context context) {
        final Resources resources = context.getResources();
        final int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0)
            return resources.getDimensionPixelSize(resourceId);
        else
            return (int) Math.ceil((Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? 24 : 25) * resources.getDisplayMetrics().density);
    }

}