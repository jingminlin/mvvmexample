package net.jinfm.app.mvvmexample.jingmintest.model

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import net.jinfm.app.mvvmexample.jingmintest.network.NewsPaperApi

class PaperViewModel: ViewModel() {

    companion object {
        private const val TAG = "PaperViewModel"
    }

    private val _newsPapers = MutableLiveData<List<NewsPaperData>>()
    val newsPapers: LiveData<List<NewsPaperData>>
        get() = _newsPapers

    private val _status = MutableLiveData<LoadApiStatus>()
    val status: LiveData<LoadApiStatus>
        get() = _status

    private val _error = MutableLiveData<String>()
    val error: LiveData<String>
        get() = _error

    private var viewModelJob = Job()
    // CoroutineScope 包含 JOB
    // MainScope 的 CoroutineContext 是 supervisorJob() + Dispatcher.Main
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    init {
        getNewsPapersResult(true)
        Log.d(TAG, "!!!!!!! ")
    }

    private fun getNewsPapersResult(isInitial: Boolean = false) {
        coroutineScope.launch {
            Log.d(TAG, "~~~~~ ")
            val result = getNewsPapers()

            when (result) {
                is Result.Success -> {
                    _error.value = null
                    if (isInitial) _status.value = LoadApiStatus.DONE
                    result.data
                }
                is Result.Fail -> {
                    Log.d(TAG, "${result.error} ")
                    _error.value = result.error.toString()
                    if (isInitial) _status.value = LoadApiStatus.ERROR
                    null
                }
                is Result.Error -> {
                    Log.d(TAG, "$result. ")
                    _error.value = result.exception.toString()
                    if (isInitial) _status.value = LoadApiStatus.ERROR
                    null
                }
                else -> {
                    if (isInitial) _status.value = LoadApiStatus.ERROR
                    null
                }
            }.also { _newsPapers.value = it }
        }
    }

    // suspend 可以被暫停
    // suspend 必須要在 coroutineScope 的 launch 、runBlocking 內或者是其他的 suspend method 才能夠被使用
    suspend fun getNewsPapers(): Result<List<NewsPaperData>> {
        return try {
            val listPaperResult = NewsPaperApi.retrofitService.getNewsPapers()

            Log.d(TAG, "listPaperResult $listPaperResult ")
            Result.Success(listPaperResult)

        } catch (e: Exception) {
            Log.d(TAG, "${this::class.simpleName} exception=${e.message}")
            Result.Error(e)
        }
    }


}