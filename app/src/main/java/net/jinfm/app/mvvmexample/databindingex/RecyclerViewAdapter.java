package net.jinfm.app.mvvmexample.databindingex;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import net.jinfm.app.mvvmexample.R;
import net.jinfm.app.mvvmexample.databinding.LayoutItemBinding;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<BookModel> books;

    public RecyclerViewAdapter(List<BookModel> books) {
        this.books = books;
    }

    public RecyclerViewAdapter(List<BookModel> books, OnItemClickListener listener) {
        this.books = books;
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(BookModel item, int position);
    }
    private OnItemClickListener listener = null;
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        // 通過 DataBindingUtil.inflate 實例化布局
        LayoutItemBinding layoutItemBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.layout_item, parent, false);

        return new MyViewHolder(layoutItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookModel book = books.get(position);
        // 設置布局變量的值
        ((MyViewHolder) holder).layoutItemBinding.setBookModel(book);

        ((MyViewHolder) holder).layoutItemBinding.getRoot().findViewById(R.id.icon).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(view.getContext(), "第 " + position + " 張圖", Toast.LENGTH_SHORT).show();
                    }
                }
        );

        ((MyViewHolder) holder).layoutItemBinding.getRoot().setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        Toast.makeText(view.getContext(), "第 " + position + " 個項目", Toast.LENGTH_SHORT).show();

                        // 把參數傳給外面，由外面決定要做的事情
                        if (listener != null) {
                            listener.onItemClick(((MyViewHolder) holder).layoutItemBinding.getBookModel(), position);
                        }

                    }
                }
        );


    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        LayoutItemBinding layoutItemBinding;

        public MyViewHolder(LayoutItemBinding itemView) {
            // getRoot() 返回的是布局文件的最外層 UI 視圖
            super(itemView.getRoot());
            layoutItemBinding = itemView;
        }
    }
}
