package net.jinfm.app.mvvmexample.paging.positionaldatasource.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Chapters {
    /**
     * 返回當前數量
     */
    public int count;

    /**
     * 起始位置
     */
    public int start;

    /**
     * 一共多少數據
     */
    public int total;

    /**
     * 返回的目錄列表
     */
    @SerializedName("list")
    public List<Chapter> chapterList;
}
