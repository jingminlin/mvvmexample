package net.jinfm.app.mvvmexample.room;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

/**
 * 第六章：Room
 * 定義 Dao 接口文件，以便對 Entity 進行讀寫
 */
@Dao
interface StudentDao {
    @Insert
    void insertStudent(Student student);

    @Delete
    void deleteStudent(Student student);

    @Update
    void updateStudent(Student student);

    @Query("SELECT * FROM student")
    List<Student> getStudentList();

    @Query("SELECT * FROM student WHERE id = :id")
    Student getStudentById(int id);

    @Query("DELETE FROM student WHERE id = :id")
    void deleteStudentById(int id);

    @Query("DELETE FROM student")
    void deleteStudentAll();

    /**
     * 第六章：Room
     * Room 與 LiveData、ViewModel 結合使用
     */
    @Query("SELECT * FROM student")
    LiveData<List<Student>> getStudentList_LiveData();

}
