package net.jinfm.app.mvvmexample.paging.positionaldatasource;

import net.jinfm.app.mvvmexample.paging.positionaldatasource.model.Chapter;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

/**
 * 第九章：Paging
 * ChapterDataSourceFactory 負責創建 ChapterDataSource
 * 並使用 LiveData 包裝 ChapterDataSource
 * 將其暴露給 ChapterViewModel
 *
 */
public class ChapterDataSourceFactory extends DataSource.Factory<Integer, Chapter>{

    private MutableLiveData<ChapterDataSource> liveDataSource = new MutableLiveData<>();

    @NonNull
    @Override
    public DataSource<Integer, Chapter> create() {
        ChapterDataSource dataSource = new ChapterDataSource();
        liveDataSource.postValue(dataSource);
        return dataSource;
    }
}
