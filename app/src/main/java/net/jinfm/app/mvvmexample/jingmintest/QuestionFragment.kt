package net.jinfm.app.mvvmexample.jingmintest

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import net.jinfm.app.mvvmexample.databinding.FragmentQuestionBinding

class QuestionFragment : Fragment() {

    lateinit var dataBinding: FragmentQuestionBinding

    companion object {

        private const val TAG = "QuestionFragment"
//        private var activity: Activity? = null

        @JvmStatic
        fun newInstance(activity: Activity?): QuestionFragment {
            val QuestionFragment = QuestionFragment()
//            this.activity = activity
            return QuestionFragment
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        dataBinding = FragmentQuestionBinding.inflate(inflater, container, false)
        dataBinding.lifecycleOwner = this

        initData()

        return dataBinding.root

    }

    private fun initData() {

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
            initView()
            initHandler()
    }

    private fun initView() {
        dataBinding.wbvQuestion.loadUrl("https://lab7-misc.udn.com/AppFeedMaker/vipQA")

    }

    private fun initHandler() {

    }

}
