package net.jinfm.app.mvvmexample;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.jinfm.app.mvvmexample.tool.TimerViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

public class secondFragment extends Fragment {

    private String TAG = this.getClass().getName();
    private View view;
    private static Context mContext;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_second, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        init();

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        // 第三章：Navigation App Bar
        // App bar 是在 MainActivity 中進行管理的
        // 當從 MainFragment 跳轉到 SecondFragment 時
        // 需要覆蓋此方法，並清出 MainFragment 所對應的 menu
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void init() {

        Bundle bundle = getArguments();
        if (bundle != null) {
            String userName = MainFragmentArgs.fromBundle(getArguments()).getUserName();
            int age = MainFragmentArgs.fromBundle(getArguments()).getAge();
            Log.d(TAG, "userName: " + userName); // "unknown"
            Log.d(TAG, "age: " + age); // 0
            String deepLinkParams = bundle.getString("params");
            Log.d(TAG, "deepLinkParams: " + deepLinkParams); // ParamsFromUrl_HelloMichael

        }

        view.findViewById(R.id.btnToRoomFragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_secondFragment);
                Navigation.findNavController(v).navigate(R.id.action_mainFragment_to_roomFragment, bundle);
            }
        });

        // 第五章：ViewModel ---------
        final TextView tvTime_LiveData = view.findViewById(R.id.tvTime_LiveData);
        // 通過 ViewModelProvider 得到 ViewModel
        // 注意：這裡的參數需要的是 Activity，而不是 Fragment，否則將收不到監聽
//        TimerViewModel timerViewModel_LiveData = new ViewModelProvider(this).get(TimerViewModel.class);
        TimerViewModel timerViewModel_LiveData = new ViewModelProvider(this.getActivity()).get(TimerViewModel.class);
        // 得到 ViewModel 中的 LiveData
        final MutableLiveData<Integer> liveData = (MutableLiveData<Integer>) timerViewModel_LiveData.getCurrentSecond();
        // 方法一：通過 LiveData.observe() 觀察 ViewModel 中的數據變化
//        liveData.observe(getViewLifecycleOwner(), new Observer<Integer>() {
//            @Override
//            public void onChanged(Integer integer) {
//                // 收到回調後更新 UI 介面
//                tvTime_LiveData.setText("Time :" + integer);
//                // 只有在 Lifecycle.State.ON_STARTED 或 Lifecycle.State.ON_RESUME 時，頁面才能收到來自 LiveDate 的通知
//                Log.d(TAG, "第五章：LiveData observe" + integer);
//            }
//        });
        // 方法二：通過 LiveData.observeForever() 觀察 ViewModel 中的數據變化
        liveData.observeForever(new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                tvTime_LiveData.setText("Time :" + integer);
                // Lifecycle.State.ON_STARTED 或 Lifecycle.State.ON_RESUME 和其他狀態，頁面都能收到來自 LiveDate 的通知
//                Log.d(TAG, "第五章：LiveData observeForever " + integer);
//                liveData.removeObserver(this);
            }
        });
//        // 前一頁已啟動過了
//        timerViewModel_LiveData.startTiming();
        // ---------------------

    }
}
