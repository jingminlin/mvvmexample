package net.jinfm.app.mvvmexample.tool;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

/**
 * 第二章：LifeCycle
 */
public class MyLocationListener implements LifecycleObserver {

    final String TAG = this.getClass().getName();
    private MyLocationListener.OnLocationChangedListener onLocationChangedListener;
    private int data = 0;
    private String str = "";

    public MyLocationListener(Context context, String str, OnLocationChangedListener onLocationChangedListener) {
        Log.d(TAG, "第二章：" + str + " initLocationManager 初始化操作 ");
        this.onLocationChangedListener = onLocationChangedListener;
        this.str = str;
    }

    /**
     * 當地理位置發生變化時，通過該接口通知調用者
     */
    public interface OnLocationChangedListener
    {
        void onChanged(int data);
    }

    private void callback(int data) {
        onLocationChangedListener.onChanged(data);
    }

    /**
     * 當 Activity 執行 onResume() 方法時，該方法會被自動調用
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private void startGetLocation() {
        Log.d(TAG, "第二章：" + str + " startGetLocation 當 onResume() 時被調用");
        data = data + 1;
        callback(data);
    }

    /**
     * 當 Activity 執行 onPause() 方法時，該方法會被自動調用
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private void pauseGetLocation() {
        Log.d(TAG, "第二章：" + str + " pauseGetLocation 當 onPause() 時被調用");
        data = data + 1;
        callback(data);
    }

}
