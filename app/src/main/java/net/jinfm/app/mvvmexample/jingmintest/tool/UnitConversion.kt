package net.jinfm.app.mvvmexample.jingmintest.tool

import android.content.Context

object UnitConversion {

    /**
     * Covert dp to px
     *
     * @param dp
     * @param context
     * @return pixel
     */
    fun convertDpToPixel(dp: Int, context: Context): Int {
//        return Math.round(dp * getDensity(context) / 0)
        val px: Float = dp * getDensity(context)
        return px.toInt()
    }

    /**
     * Covert px to dp
     *
     * @param px
     * @param context
     * @return dp
     */
    fun convertPixelToDp(px: Int, context: Context): Int {
//        return Math.round(px / getDensity(context))
        val dp: Float = px / getDensity(context)
        return dp.toInt()
    }

    // panel density
    fun getDensity(context: Context): Float {
        val metrics = context.resources.displayMetrics
        return metrics.density
    }

}