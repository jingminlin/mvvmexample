package net.jinfm.app.mvvmexample.jingmintest.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NewsPaperData(
    var _id: String,
    var present_date: String,
    var paper_name: String,
    var page_id: String,
    var page_order: String,
    var page_name: String,
    var image_path: String,
    var entity: List<NewsPaperItem>,
    var publish_time: String
): Parcelable

@Parcelize
data class NewsPaperItem(
    var _id: String,
    var art_id: Int,
    var art_title: String,
    var art_url: String,
    var position: List<Position>,
    var weight: Int
): Parcelable

@Parcelize
data class Position(
    var art_id: Int,
    var height: Int,
    var width: Int,
    var x: Int,
    var y: Int
): Parcelable
