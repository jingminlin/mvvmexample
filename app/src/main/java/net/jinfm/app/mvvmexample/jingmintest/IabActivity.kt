package net.jinfm.app.mvvmexample.jingmintest

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import net.jinfm.app.mvvmexample.R
import net.jinfm.app.mvvmexample.databinding.ActivityIabBinding
import net.jinfm.app.mvvmexample.jingmintest.model.ServerIABData

class IabActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "IabActivity"
        private var serverIABData: ServerIABData? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityIabBinding>(this, R.layout.activity_iab)
        binding.lifecycleOwner = this

        var bundle: Bundle? = getIntent().getExtras()
        var param = bundle?.getString("param").toString()
//        serverIABData = bundle?.getParcelable("ServerIABData") as LiveData<ServerIABData>?
        serverIABData = bundle?.getParcelable("ServerIABData") as ServerIABData?
        Log.d(TAG, "param $param")

        init()
    }

    fun init() {
//        val fragment = IabFragment.newInstance(application,this)
        val fragment = IabFragment.newInstance(this)
        supportFragmentManager.beginTransaction()
                .replace(R.id.container_iab, fragment, IabFragment::class.java.simpleName)
//                .addToBackStack(null)
                .commitAllowingStateLoss()

    }

//    override fun onBackPressed() {
//        val fragment: Fragment? = supportFragmentManager.findFragmentById(R.id.container_paper)
//        if (fragment is PaperContentFragment) {
//            supportFragmentManager.popBackStack()
//        }
//    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}