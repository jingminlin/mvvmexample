package net.jinfm.app.mvvmexample.jingmintest.network

import net.jinfm.app.mvvmexample.jingmintest.model.UserData
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("/users/1") //annotation 註解宣告方式定義 HTTP 連線獲取資料方法與指定API後網址
    open fun getUser(): Call<UserData>
//    suspend fun getNewsPapers(): List<NewsPaperData>

    @GET("/users") //annotation 註解宣告方式定義 HTTP 連線獲取資料方法與指定API後網址
    open fun getUsers(): Call<List<UserData>>

}

