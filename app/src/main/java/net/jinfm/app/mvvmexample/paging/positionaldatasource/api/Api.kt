package net.jinfm.app.mvvmexample.paging.positionaldatasource.api

import net.jinfm.app.mvvmexample.paging.positionaldatasource.model.Chapters
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * 第九章：Paging
 */
public interface Api {
    /**
     * 獲取目錄資料
     */
    @GET("ShowStoreProductChapter")
    open fun getChapters(
            @Query("id") id: Int,
            @Query("order_by") order_by: String,
            @Query("amount_per_page") amount_per_page: Int,
            @Query("page") page: Int
    ): Call<Chapters>
}