package net.jinfm.app.mvvmexample.paging.positionaldatasource;

import android.app.Application;
import android.os.AsyncTask;

import net.jinfm.app.mvvmexample.paging.positionaldatasource.api.RetrofitClient;
import net.jinfm.app.mvvmexample.paging.positionaldatasource.db.ChapterDatabase;
import net.jinfm.app.mvvmexample.paging.positionaldatasource.model.Chapter;
import net.jinfm.app.mvvmexample.paging.positionaldatasource.model.Chapters;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.paging.PagedList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChapterBoundaryCallback extends PagedList.BoundaryCallback<Chapter> {

    private String TAG = this.getClass().getName();
    private Application application;
    public static final int amount_per_page = 10;
    private int page = 1;

    public ChapterBoundaryCallback(Application application) {
        this.application = application;
    }

    /**
     * 當數據為空時，會調用該方法，請求第一頁的數據
     */
    @Override
    public void onZeroItemsLoaded() {
        super.onZeroItemsLoaded();
        getTopData();
    }

    @Override
    public void onItemAtFrontLoaded(@NonNull Chapter itemAtFront) {
        super.onItemAtFrontLoaded(itemAtFront);
    }

    /**
     * 當用戶滑動到 RecyclerView 底部且數據全部加載完畢，會通知 onItemAtEndLoaded 方法，請求下一頁的數據
     * itemAtEnd 此參數為最後一條數據
     * @param itemAtEnd
     */
    @Override
    public void onItemAtEndLoaded(@NonNull Chapter itemAtEnd) {
        super.onItemAtEndLoaded(itemAtEnd);
        getTopAfterData(itemAtEnd);
    }

    /**
     * 加載第一頁數據
     */
    private void getTopData() {
        RetrofitClient.getInstance()
                .getApi()
                .getChapters(212910, "chapter", amount_per_page, page)
                .enqueue(new Callback<Chapters>() {
                    @Override
                    public void onResponse(Call<Chapters> call, Response<Chapters> response) {
                        if (response.body() != null) {
                            insertChapters(response.body().chapterList);
                            page = page + 1;
                        }
                    }

                    @Override
                    public void onFailure(Call<Chapters> call, Throwable t) {

                    }
                });
    }

    /**
     * 加載下一頁數據
     */
    private void getTopAfterData(Chapter chapter) {
        RetrofitClient.getInstance()
                .getApi()
                .getChapters(212910, "chapter", amount_per_page, page)
                .enqueue(new Callback<Chapters>() {
                    @Override
                    public void onResponse(Call<Chapters> call, Response<Chapters> response) {
                        if (response.body() != null){
                            insertChapters(response.body().chapterList);
                            page = page + 1;
                        }
                    }

                    @Override
                    public void onFailure(Call<Chapters> call, Throwable t) {

                    }
                });
    }

    /**
     * 插入數據
     */
    private void insertChapters(final List<Chapter> chapters){

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                ChapterDatabase.getInstance(application)
                        .chapterDao()
                        .insertChapters(chapters);
            }
        });
    }
}
