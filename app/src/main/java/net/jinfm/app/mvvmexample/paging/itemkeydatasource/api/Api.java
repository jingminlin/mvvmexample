package net.jinfm.app.mvvmexample.paging.itemkeydatasource.api;


import net.jinfm.app.mvvmexample.paging.itemkeydatasource.model.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * 第九章：Paging
 */
public interface Api {
    @GET("users")
    Call<List<User>> getUsers(
            @Query("since") int since,
            @Query("per_page") int perPage
    );

    @GET("users")
    Call<ResponseBody> getUsers_string(
            @Query("since") int since,
            @Query("per_page") int perPage
    );

    @GET("users")
    Call<ResponseBody> getUsers_string();

}