package net.jinfm.app.mvvmexample.paging.pagekeyeddatasource;

import net.jinfm.app.mvvmexample.paging.pagekeyeddatasource.api.RetrofitClient;
import net.jinfm.app.mvvmexample.paging.pagekeyeddatasource.model.User;
import net.jinfm.app.mvvmexample.paging.pagekeyeddatasource.model.UserResponse;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDataSource extends PageKeyedDataSource<Integer, User> {

//    public static final int FIRT_PAGE = 1;
    private int page = 1;
    public static final int PER_PAGE = 20;
    public static final String SITE = "stackoverflow";

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, User> callback) {
        RetrofitClient.getInstance()
                .getApi()
                .getUsers(page, PER_PAGE, SITE)
                .enqueue(new Callback<UserResponse>() {
                    @Override
                    public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                        if (response.body() != null){
                            /**
                             * 第一個參數是加載得到的數據，傳給 PagedList
                             * 第二個參數式上一頁的 key
                             * 第三個參數是下一頁的 key
                             * 加載第一頁不存在上一頁，傳 null
                             * 下一頁為當頁 +1
                             */
                            page = page +1;
                            callback.onResult(response.body().users, null, page);
                        }
                    }

                    @Override
                    public void onFailure(Call<UserResponse> call, Throwable t) {

                    }
                });
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, User> callback) {
        // 暫時用不到
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, User> callback) {
        RetrofitClient.getInstance()
                .getApi()
                .getUsers(page, PER_PAGE, SITE)
                .enqueue(new Callback<UserResponse>() {
                    @Override
                    public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                        if (response.body() != null){
                            // 若無下一頁的資料，則設定為 null
                            Integer nextKey = response.body().hasMore ? params.key + 1 : null;
                            page = nextKey;
                            callback.onResult(response.body().users,  page);
                        }
                    }

                    @Override
                    public void onFailure(Call<UserResponse> call, Throwable t) {

                    }
                });
    }
}
