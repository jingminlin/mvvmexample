package net.jinfm.app.mvvmexample.tool;

import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import static android.content.ContentValues.TAG;

public class TimerViewModel extends ViewModel {

    private Timer timer, timer_LiveData;
    private int currentSecond;
    private AndroidViewModel androidViewModel;
//        androidViewModel.getApplication()


    // 第五章：5.6 節：實現 Freagment 之間的通訊
    private MutableLiveData<Integer> progress;
    public LiveData<Integer> getProgress() {
        if (progress == null) {
            progress = new MutableLiveData<>();
        }
        return progress;
    }

    // 第五章：LiveData ---------
    /**
     * LiveData 是一個抽象類不能直接使用，要使用它的直接子類 MutableLiveData
     */
    public MutableLiveData<Integer> currentSecond_LiveData;
    public LiveData<Integer> getCurrentSecond() {
        if (currentSecond_LiveData == null) {
            currentSecond_LiveData = new MutableLiveData<>();
        }
        return currentSecond_LiveData;
    }
    /**
     * 開始計時
     */
    public void startTiming_LiveData() {
        if (timer_LiveData == null) {
            currentSecond_LiveData.setValue(0);
            timer_LiveData = new Timer();
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    currentSecond_LiveData.postValue(currentSecond_LiveData.getValue() + 1);
                }
            };
            timer_LiveData.schedule(timerTask, 1000, 1000);
        }
    }
    // -------------------------

    /**
     * 開始計時
     */
    public void startTiming() {
        if (timer == null) {
            currentSecond = 0;
            timer = new Timer();
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    currentSecond++;
                    if (onTimeChangeListener != null) {
                        onTimeChangeListener.onTimeChanged(currentSecond);
                    }
                }
            };
            timer.schedule(timerTask, 1000, 1000);
        }
    }

    /**
     * 通過接口的方式，完成對調用者的通知
     * 而更好的方式是通過 LiveData 來實現，後面會實作
     */
    public interface OnTimeChangeListener{
        void onTimeChanged(int second);
    }
    private OnTimeChangeListener onTimeChangeListener;
    public void setOnTimeChangeListener(OnTimeChangeListener onTimeChangeListener) {
        this.onTimeChangeListener = onTimeChangeListener;
    }

    /**
     * 清理資源
     */
    @Override
    protected void onCleared() {
        super.onCleared();
        try {

            Log.d(TAG, "onCleared");
            timer.cancel();
            timer_LiveData.cancel();

            // 第五章：5.6 節：實現 Freagment 之間的通訊
            progress = null;

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
