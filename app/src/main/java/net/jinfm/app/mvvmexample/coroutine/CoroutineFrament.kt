package net.jinfm.app.mvvmexample.coroutine

import android.app.Activity
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.coroutines.*
import net.jinfm.app.mvvmexample.databinding.FragmentCoroutineBinding
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.concurrent.TimeUnit
import kotlin.coroutines.CoroutineContext
import kotlin.system.measureTimeMillis

class CoroutineFrament : Fragment(), CoroutineScope by MainScope() {

    /**
     * 參考網址：
    https://medium.com/jastzeonic/kotlin-coroutine-%E9%82%A3%E4%B8%80%E5%85%A9%E4%BB%B6%E4%BA%8B%E6%83%85-685e02761ae0
    https://picapro33.medium.com/%E4%BD%BF%E7%94%A8-kotlin-coroutines-%E9%96%8B%E7%99%BC-android-app-%E5%BF%AB%E9%80%9F%E4%B8%8A%E6%89%8B-77c1b7243c6a
     */

    /*
    * Scope 、CoroutineContext 、Job 三者的關係。
    * 最基礎的情況會是 Scope 包含了 CoroutinContext，
    * 而 CoroutineContext 裡面包含了 Job。
    */

    /**
     * Dispatchers.Main：就是 Main thread 的包裝，在 Android 需要操作到 UI thread 通常會用它
     * Dispatchers.Default：也就是預設，這裡容易和 CoroutineStart.DEFAULT 搞混，事實上它的意思是 Dispatchers 的預設，就是執行預設的 CoroutineScheduler，原則上是會開另一個 Thread，通常不會跑在 Main thread 上。
     * Dispatchers.IO：原則上它是基於 Default 去加強的 Dispatchers ，它跟 Default 最本質的區別在於 ，它開的 thread 數量會比較多，以 Default 來說是 N ( JVM 給的數量，個平台機子會不同)，而 IO 則會給上 64 個，如果 JVM 給更多，那就更多那樣。那它與 Default 都是負責不把 Main thread 堵注的耗時處理。
     * Dispatchers.Unconfined：unconfined 有不受限制之意，通常他會跑在執行該 Coroutines 的 thread 上，但是在 suspend (暫停，之後會提到)後被回復可能會跑到另一個 thread 上，使用上需要注意。
     */

    // C. 配合 Activity 的生命週期
    // CoroutineScope by MainScope()
    override fun onDestroy() {
        super.onDestroy()
        cancel()
    }

    // A. 直接實作 CoroutineScope
    // CoroutineContext 指的就是 Coroutines 作用的情景
    // 這邊可以指定他是在 Main thread 上或者就直接弄一個 Job 給他
    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = job
    // 如果沒有特別定義，那這個 Job 就是跑在 Worker thread 上了，用它更新 UI 會噴 Exception
//        get() = TODO("not implemented")

    // B. 配合 View 的生命週期
//    private val scopeView = MainScope()
//    override fun onDestroy() {
//        super.onDestroy()
//        scopeView.cancel()
//    }

    lateinit var binding: FragmentCoroutineBinding

    companion object {
        private const val TAG = "CoroutineFrament"
        private var activity: Activity? = null

        @JvmStatic
        fun newInstance(activity: Activity): CoroutineFrament {
            val CoroutineFrament = CoroutineFrament()
            this.activity = activity
            return CoroutineFrament
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCoroutineBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this

        // 資料
        initData()

        return binding.root
    }

    private fun initData() {

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // UI
        initView()
        // 動作
        initHandler()
    }

    // UI
    private fun initView() {

        initRootView()

    }

    // 動作
    private fun initHandler() {

//        testCoroutines()

        testAsync()

        button_listner()

    }

    // -------------------------

    private fun initRootView() {

    }

    private fun testCoroutines() {

        // 倒數的 Text ，用 Coroutines 可以簡單地做到

        // 多線程方法一：Thread
        Thread {
            for (i in 10 downTo 1) {
                Thread.sleep(1000)
                activity?.runOnUiThread {
                    Log.d(TAG, "Thread i $i")
                }
            }
            activity?.runOnUiThread {
                Log.d(TAG, "Done!")
            }
        }.start()

        // 多線程方法二：Coroutines
        GlobalScope.launch(Dispatchers.Main) {
            for (i in 10 downTo 1) {
                Log.d(TAG, "Coroutines i $i")
                binding.tvTime.text = "count down $i ..." // update text
                delay(1000)
            }
            binding.tvTime.text = "Done!"
        }

        // 多線程方法三：Coroutines + 停住
        // Job 指的是 單一個 Coroutines 的生命週期
        // launch 如果沒有傳入參數，則會使用 CoroutineScope 所定義的 CoroutineContext
        val job1: Job = GlobalScope.launch(Dispatchers.Main) {
            // launch coroutine in the main thread
            for (i in 10 downTo 1) { // countdown from 10 to 1
                Log.d(TAG, "Coroutines i $i");
                delay(1000) // wait half a second
            }
            Log.d(TAG, "Done!")
        }
        // 停住單一 Coroutines
        job1.cancel()

    }

    private fun testAsync() {

        /*
        * async 可以直接透過 scope 調用，那它跟 launch 最大的不同是調用它會回傳的是一個叫做 Deferred 的玩意，而非 Job。
        * Deferred 緩徵，概念有點類似 future ，這樣寫的好處是可以只耗費一次長時間呼叫，
        * 那具體要怎麼取得這個資料呢？Deferred 有提供一個 method，叫做 await()
        */

        val rawText: Deferred<String> = GlobalScope.async(Dispatchers.IO) {
            "result"
        }

        // await 不能直接調用，因為它是 Suspend function ，需要透過 Coroutine 調用
        // rawText.await()
        GlobalScope.launch {
            val text = rawText.await()
//            println(text)
            Log.d(TAG, "testAsync text $text")
        }

        Thread.sleep(100) //Keep process alive

        // --------------------------------------------------------------------------


        main() // 併發的(Concurrency)
        main1() // 併發的(Concurrency)
        main2() // 串聯的

    }

    /**
     * 併發的(Concurrency)
     * 原先應該會跑上 2 秒，但是使用 Async 只花了一秒多，很明顯是並發的
     *
     * 他跟 launch 很像，同樣都是 async 之後會立刻執行該 block ，
     * 差別在 launch 沒有 deferred 保住最後的結果，而有了 Deferred 也可以利用 Deferred 等待結果，
     * 所以若在一個 Coroutines 可以利用 async 打出多個需要併發耗時作業，再利用 await 等待結果，以達到併發的效果
     *
     * runBlocking 它的目的是確保流程的串聯而非並聯
     */
    fun main() = runBlocking {
        val time = measureTimeMillis {
            val one = async { doSomethingUsefulOne() }
            val two = async { doSomethingUsefulTwo() }
//            println("The answer is ${one.await() + two.await()}")
            Log.d(TAG, "The answer is ${one.await() + two.await()}")
        }
//        println("Completed in $time ms")
        Log.d(TAG, "Completed in $time ms")
    }

    suspend fun doSomethingUsefulOne(): Int {
        delay(1000L) // pretend we are doing something useful here
        return 13
    }

    suspend fun doSomethingUsefulTwo(): Int {
        delay(1000L) // pretend we are doing something useful here, too
        return 29
    }

    // 併發的(Concurrency)
    fun main1() {

        launch(Dispatchers.IO){
            delay(300)
//            println("main1 runBlocking 1")
            Log.d(TAG, "main1 runBlocking 1")
        }


        launch(Dispatchers.IO){
            delay(100)
//            println("main1 runBlocking 2")
            Log.d(TAG, "main1 runBlocking 2")

        }

        Thread.sleep(1000) //Keep process alive
    }

    // 串聯的
    fun main2() {

        runBlocking(Dispatchers.IO){
            delay(300)
            Log.d(TAG, "main2 runBlocking 1")
        }

        runBlocking(Dispatchers.IO){
            delay(100)
            Log.d(TAG, "main2 runBlocking 2")

        }

        Thread.sleep(1000) //Keep process alive
    }

    fun button_listner() {

        binding.btnApi.setOnClickListener {

//            // baseUrl: 定義 API 網域網址，即是我們剛剛拆出來的前綴共用的部分
//            // ddConverterFactory: 定義要將資料轉成什麼格式
//            val retrofit = Retrofit.Builder()
//                    .baseUrl("https://jsonplaceholder.typicode.com")
//                    .addConverterFactory(MoshiConverterFactory.create())
//                    .build()
//
//            val apiService: ApiService = retrofit.create(ApiService::class.java)
//
//            //宣告 Call 連線服務
////            val call: Call<UserData> = apiService.getUser()
//            val call: Call<List<UserData>> = apiService.getUsers()
//
//            //執行連線服務，透過 Callback 來等待回傳成功或失敗的資料
//            call.enqueue(object : Callback<List<UserData>?> {
//                override fun onResponse(call: Call<List<UserData>?>?, response: Response<List<UserData>?>) {
//                    // 連線成功，透過 getter 取得特定欄位資料
//                    try {
//
////                        for (i in response.body()?.size?.downTo(0)!!) {
////                            Log.d(TAG, "id: " + response.body()?.get(i).id)
////                            Log.d(TAG, "name: " + response.body()?.get(i).name)
////
////                        }
//
////                        for (i in 0 until response.body()?.size!!){
////                            println(response.body()!!.get(i).id)
////                        }
//
//                        // 透過 foreach 取出每一筆資料內容
//                        for (item in response.body()!!) {
//                            Log.d(TAG, "id: " + item.id)
//                            Log.d(TAG, "name: " + item.name)
//                        }
//
////                        Log.d(TAG, "id: " + response.body().get(i).id)
////                        Log.d(TAG, "name: " + response.body().get(i).name)
//
////                        Log.d(TAG, "id: " + response.body()?.id)
////                        Log.d(TAG, "name: " + response.body()?.name)
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }
//                }
//
//                override fun onFailure(call: Call<List<UserData>?>?, t: Throwable) {
//                    // 連線失敗，印出錯誤訊息
//                    Log.d(TAG, "response: $t")
//                }
//
//            })


            // ------------------------------------------------------------------------

            getUpdateConfig().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)

//            val serverIABDataViewModel = ViewModelProvider(this).get(ServerIABDataViewModel::class.java)
//            serverIABDataViewModel.ServerIabData.observe(viewLifecycleOwner, Observer {
//                Log.d(TAG, "serverIABData: $it")
//            })

//            val paperViewModel = ViewModelProvider(this).get(PaperViewModel::class.java)
//            paperViewModel.newsPapers.observe(viewLifecycleOwner, Observer {
//                Log.d(TAG, "newsPapers: $it")
//            })

            // ------------------------------------------------------------------------

//            // baseUrl: 定義 API 網域網址，即是我們剛剛拆出來的前綴共用的部分
//            // ddConverterFactory: 定義要將資料轉成什麼格式
//            val retrofit = Retrofit.Builder()
//                    .baseUrl("https://lab7-misc.udn.com/AppFeedMaker/")
//                    .addConverterFactory(MoshiConverterFactory.create().asLenient())
//                    .build()
//
//            val iabPageApiService: IabPageApiService = retrofit.create(IabPageApiService::class.java)
//
//            //宣告 Call 連線服務
////            val call: Call<List<ServerIABData>> = iabPageApiService.getServerIabData_test()
////            //執行連線服務，透過 Callback 來等待回傳成功或失敗的資料
////            call.enqueue(object : Callback<List<ServerIABData>?> {
////                override fun onResponse(call: Call<List<ServerIABData>?>?, response: Response<List<ServerIABData>?>) {
////                    try {
////                        for (item in response.body()!!) {
////                            Log.d(TAG, "storeId: " + item.storeId)
////                        }
////                    } catch (e: Exception) {
////                        e.printStackTrace()
////                    }
////                }
////                override fun onFailure(call: Call<List<ServerIABData>?>?, t: Throwable) {
////                    Log.d(TAG, "response: $t")
////                }
////            })
//
//            val call: Call<ResponseBody?>? = iabPageApiService.getServerIabData_test("vipProducts")
//
//            call?.enqueue(object : Callback<ResponseBody?> {
//                override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
//                    try {
//                        Log.d(TAG, "response.headers(): " + response.headers())
//                        Log.d(TAG, "response.code(): " + response.code())
//                        Log.d(TAG, "response.body(): " + response.body())
//                        Log.d(TAG, "response.message(): " + response.message())
//                        Log.d(TAG, "response.raw(): " + response.raw())
//                        Log.d(TAG, "response.raw().body: " + response.raw().body)
//                        Log.d(TAG, "response: " + response)
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }
//                }
//
//                override fun onFailure(call: Call<Response?>, t: Throwable) {
//                    Log.d(TAG, "response: $t")
//                }
//
//            })



        }
    }

    class getUpdateConfig internal constructor() : AsyncTask<Void?, Void?, JSONObject?>() {

        private val configUrl: String? = "https://lab7-misc.udn.com/AppFeedMaker/vipProducts"

        init {

        }

        override fun doInBackground(vararg params: Void?): JSONObject? {
            if (configUrl != null) {
                val client: OkHttpClient = OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build()
                val request: Request = Request.Builder().url(configUrl).build()

                try {
                    val response = client.newCall(request).execute()
                    val responseBody = response.body
                    if (responseBody != null) {
                        val result = responseBody.string()
                        val resultObject = JSONObject(result)
                        return resultObject
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
            return null
        }

        override fun onPostExecute(result: JSONObject?) {
            super.onPostExecute(result)
            if (result != null) {
            }
        }

    }


}
