package net.jinfm.app.mvvmexample.jingmintest.model


import android.annotation.SuppressLint
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class ServerIABData(
    @Json(name = "products")
    val products: List<Product>,
    @Json(name = "salesInfo")
    val salesInfo: SalesInfo
) : Parcelable {
    @SuppressLint("ParcelCreator")
    @Parcelize
    data class Product(
            @Json(name = "gift")
        val gift: List<Gift>?,
            @Json(name = "product_discountprice")
        val productDiscountprice: Int,
            @Json(name = "product_name")
        val productName: String,
            @Json(name = "product_offlinetime")
        val productOfflinetime: String,
            @Json(name = "product_onlinetime")
        val productOnlinetime: String,
            @Json(name = "product_orderunit")
        val productOrderunit: String,
            @Json(name = "product_originalprice")
        val productOriginalprice: Int,
            @Json(name = "store_id")
        val storeId: String
    ) : Parcelable{
        @SuppressLint("ParcelCreator")
        @Parcelize
        data class Gift(
                @Json(name = "action_time")
                val actionTime: String,
                @Json(name = "gift_cost")
                val giftCost: Int,
                @Json(name = "gift_desc")
                val giftDesc: String,
                @Json(name = "gift_id")
                val giftId: Int,
                @Json(name = "gift_name")
                val giftName: String,
                @Json(name = "gift_url")
                val giftUrl: String,
                @Json(name = "location")
                val location: String,
                @Json(name = "max_number")
                val maxNumber: String,
                @Json(name = "presenter")
                val presenter: String,
                @Json(name = "question_url")
                val questionUrl: String,
                @Json(name = "weight")
                val weight: String?
        ) : Parcelable
    }

    @SuppressLint("ParcelCreator")
    @Parcelize
    data class SalesInfo(
        @Json(name = "banner")
        val banner: String,
        @Json(name = "info")
        val info: String
    ) : Parcelable
}