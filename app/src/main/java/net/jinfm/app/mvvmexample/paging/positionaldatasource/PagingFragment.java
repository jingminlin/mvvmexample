package net.jinfm.app.mvvmexample.paging.positionaldatasource;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.jinfm.app.mvvmexample.R;
import net.jinfm.app.mvvmexample.paging.positionaldatasource.model.Chapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class PagingFragment extends Fragment {

    private String TAG = this.getClass().getName();
    private View view;
    private SwipeRefreshLayout swipe_container;
    private RecyclerView paging_recycler_view;
    private ChapterPagedListAdapter adapter;
    private ChapterViewModel chapterViewModel;
    private ChapterViewModelBC chapterViewModelBC;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_paging, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        init();

    }

    private void init() {

        paging_recycler_view = view.findViewById(R.id.paging_recycler_view);
        paging_recycler_view.setLayoutManager(new LinearLayoutManager(getContext()));
        paging_recycler_view.setHasFixedSize(true);

        adapter = new ChapterPagedListAdapter(getContext());

        // 非 BoundaryCallback 的方法 -------------------------------------------------
//        chapterViewModel = new ViewModelProvider(getActivity()).get(ChapterViewModel.class);
//        chapterViewModel.chapterPagedList.observe(getActivity(), new Observer<PagedList<Chapter>>() {
//            @Override
//            public void onChanged(PagedList<Chapter> chapters) {
//                adapter.submitList(chapters);
//            }
//        });
//        paging_recycler_view.setAdapter(adapter);

        // BoundaryCallback & 下拉刷新 -------------------------------------------------
        chapterViewModelBC =
                new ViewModelProvider(this, new ChapterViewModelBCFactory(getActivity().getApplication()))
                        .get(ChapterViewModelBC.class);

        chapterViewModelBC.chapterPagedList.observe(getActivity(), new Observer<PagedList<Chapter>>() {
            @Override
            public void onChanged(PagedList<Chapter> chapters) {
                adapter.submitList(chapters);
            }
        });
        paging_recycler_view.setAdapter(adapter);

        swipe_container= view.findViewById(R.id.swipe_container);
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                chapterViewModelBC.refresh();
                swipe_container.setRefreshing(false);
            }
        });

        // -----------------------------------------------------------

        view.findViewById(R.id.toPageKeyedDataSource).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_PagingFragment_to_PageKeyFragment);
            }
        });

    }
}
